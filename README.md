# Setup project

- Download and install package automatically via yarn or npm. Simply type `yarn` or `npm install` in cmd.
- To run website, use `yarn start` or `npm run`

# Libraries

- **axios**: to make https request.
- **classnames**: to conditionally joining classNames together, used in `ReactTable` component. 
- **file-saver**: to save files on the client-side.
- **firebase**: to work with firebase.
- **html-entities**: to encode and decode html content.
- **html-react-parser**: to convert an HTML string to React Element(s).
- **lodash**: super helpers.
- **moment**: to work with datetime.
- **prop-types**: to check React props and similar objects.
- **react-calendar**: calendar component.
- **react-ckeditor-component**: advance editor.
- **react-notification-system**: to show push notification.
- **react-redux**: manage app's states.
- **react-router-dom**: for routing.
- **semantic-ui-react**: UI library.
- **uuid**: to generate uuid,

# Libraries confifuration

- **moment**: moment locale settings are put in `index.js` of this application.