import React, { Component } from "react";
import PropTypes from "prop-types";
import { Route, Redirect, withRouter } from "react-router-dom";

import { connect } from "react-redux";


class PrivateRoute extends Component {
  static propTypes = {
    user: PropTypes.shape({
      email: PropTypes.string.isRequired
    }).isRequired,
    path: PropTypes.string.isRequired
  };

  authSuccess = () => <Route exact path={this.props.path} render={props => (<this.props.component {...props} />)} />;

  authFailed = () => <Route exact path={this.props.path} render={props => (<Redirect to={{ pathname: "/", state: { from: props.location } }} />)} />;  

  render() {
    if (this.props.user.email !== "")
      return this.authSuccess();
    return this.authFailed();
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(PrivateRoute));