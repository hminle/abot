import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from './redux';

// components
import App from "./App";

// services
import registerServiceWorker from "./registerServiceWorker";

// import init config
import "./libs/moment.conf";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
