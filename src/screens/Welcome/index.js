import React, { Component } from "react";
import PropTypes from "prop-types";
import { Menu, Item, Button, Icon, Header, Container } from "semantic-ui-react";
import NotificationSystem from "react-notification-system";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// services
import { app, googleProvider } from "../../libs/firebase";
import { RESOURCES } from "../../libs/config";

// styles
import "./styles.css";


// images
const logo = `${RESOURCES}/logo.png`;

class Welcome extends Component {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired,
    user: PropTypes.shape({
      email: PropTypes.string.isRequired
    }).isRequired
  };

  componentDidMount() {
    // check if user has already logged in
    if (this.props.user.email !== "")
      this.props.history.push("/workspace");
  }

  errorHander = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 2
    });
  }

  successHandler = () => {
    this.props.history.push("/workspace");
    window.location.reload();
  }

  authWithGoogle = () => {
    app.auth().signInWithPopup(googleProvider).then((user, error) => {
      if (error) {
        this.errorHander(error.message);
      } else {
        this.successHandler();
      }
    });
  }

  render() {
    return (
      <div style={{marginTop: "-10px"}}>
        <div style={{backgroundImage: "linear-gradient(56deg, #75B2E0, #6AC1E2)"}} className="welcome-container">
          <div className="wrap welcome">
            <Menu secondary size="tiny" style={{ marginTop: 10, borderBottomWidth: 1, borderBottomStyle: "solid", borderBottomColor: "#91C4EA" }}>
              <Menu.Item>
                <Item>
                  <Item.Image src={logo} size="mini" />
                </Item>
              </Menu.Item>
              <Menu.Menu position="right">
                <Menu.Item>
                  <Button color="green" onClick={this.authWithGoogle}>
                    <Icon name="google" />Đăng nhập
                  </Button>
                </Menu.Item>
              </Menu.Menu>
            </Menu>
          </div>
        </div>
        <div style={{background: "#C1F1FF"}} className="welcome-container">
          <Container textAlign="center">
            <Header as="h2">
              PHẦN MỀM QUẢN LÝ
              <Header.Subheader>Kiểm định chất lượng Giáo dục.</Header.Subheader>
            </Header>
          </Container>
        </div>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(Welcome));