import React, { Component } from "react";
import { Table, Label, Popup } from "semantic-ui-react";

// components
import Criterion from "./Criterion";


export default class Evaluation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      standards: []
    }
  }

  componentWillMount() {
    const standards = [{
      key: "s1",
      stt: 1,
      name: "Tổ chức và quản lý nhà trường",
      criteria: [{
        key: "c11",
        stt: 1,
        name: "Cơ cấu tổ chức bộ máy của nhà trường theo quy định tại Điều lệ trường mầm non",
        required: true,
        indices: [{
          key: "i1",
          stt: 1,
          name: "Chỉ số A",
          content: [
            "Có thời gian công tác theo quy định tại Điều lệ trường mầm non; có bằng trung cấp sư phạm mầm non trở lên; đã được bồi dưỡng nghiệp vụ quản lý giáo dục và lý luận chính trị theo quy định."
          ],
          requirement: [
            "Hiệu trưởng có thời gian công tác liên tục trong giáo dục mầm non ít nhất là 5 năm",
            "Phó hiệu trưởng có thời gian công tác liên tục trong giáo dục mầm non ít nhất là 3 năm",
            "Hiệu trưởng, phó hiệu trưởng có bằng trung cấp sư phạm mầm non trở lên",
            "Đã qua lớp bồi dưỡng về nghiệp vụ quản lý giáo dục và lý luận chính trị theo quy định"
          ],
          help: [
            "Hồ sơ quản lý nhân sự",
            "Sơ yếu lý lịch của hiệu trưởng và phó hiệu trưởng",
            "Văn bằng, giấy chứng nhận, chứng chỉ có liên quan",
            "Báo cáo có nội dung liên quan"
          ]
        }, {
          key: "i2",
          stt: 2,
          name: "Chỉ số B",
          content: [
            "Nội dung chỉ số B (1)",
            "Nội dung chỉ số B (2)",
          ],
          requirement: [
            "Yêu cầu chỉ số B (1)",
            "Yêu cầu chỉ số B (2)",
          ],
          help: [
            "Gợi ý chỉ số B (1)",
            "Gợi ý chỉ số B (2)",
          ]
        }, {
          key: "i3",
          stt: 3,
          name: "Chỉ số C",
          content: [
            "Nội dung chỉ số C (1)",
            "Nội dung chỉ số C (2)",
          ],
          requirement: [
            "Yêu cầu chỉ số C (1)",
            "Yêu cầu chỉ số C (2)",
          ],
          help: [
            "Gợi ý chỉ số C (1)",
            "Gợi ý chỉ số C (2)",
          ]
        }]
      }, {
        key: "c12",
        stt: 2,
        name: "Lớp học, số trẻ, địa điểm trường theo quy định của Điều lệ trường mầm non",
        required: true
      }, {
        key: "c13",
        stt: 3,
        name: "Cơ cấu tổ chức và việc thực hiện nhiệm vụ của các tổ chuyên môn, tổ văn phòng theo quy định tại Điều lệ trường mầm non",
        required: false
      }, {
        key: "c14",
        stt: 4,
        name: "Chấp hành chủ trương, chính sách của Đảng, pháp luật của Nhà nước, sự lãnh đạo, chỉ đạo của cấp ủy Đảng, chính quyền địa phương và cơ quan quản lý giáo dục các cấp; bảo đảm Quy chế thực hiện dân chủ trong hoạt động của nhà  trường",
        required: true
      }, {
        key: "c15",
        stt: 5,
        name: "Quản lý hành chính, thực hiện các phong trào thi đua theo quy định",
        required: false
      }]
    }, {
      key: "s2",
      stt: 2,
      name: "Cán bộ quản lý, giáo viên, nhân viên và trẻ",
      criteria: [{
        key: "c21",
        stt: 1,
        name: "Năng lực của hiệu trưởng, phó hiệu trưởng trong quá trình triển khai các hoạt động nuôi dưỡng, chăm sóc và giáo dục trẻ",
        required: true
      }, {
        key: "c22",
        stt: 2,
        name: "Số lượng, trình độ đào tạo và yêu cầu về kiến thức của giáo viên",
        required: true
      }]
    }, {
      key: "s3",
      stt: 3,
      name: "Cơ sở vật chất, trang thiết bị, đồ dùng, đồ chơi",
      criteria: [{
        key: "c31",
        stt: 1,
        name: "Diện tích, khuôn viên và các công trình của nhà trường theo quy định tại Điều lệ trường mầm non"
      }, {
        key: "c32",
        stt: 2,
        name: "Sân, vườn và khu vực cho trẻ chơi bảo đảm yêu cầu"
      }]
    }, {
      key: "s4",
      stt: 4,
      name: "Quan hệ giữa nhà trường, gia đình và xã hội",
      criteria: [{
        key: "c41",
        stt: 1,
        name: "Năng lực của hiệu trưởng, phó hiệu trưởng trong quá trình triển khai các hoạt động nuôi dưỡng, chăm sóc và giáo dục trẻ",
        required: true
      }, {
        key: "c42",
        stt: 2,
        name: "Số lượng, trình độ đào tạo và yêu cầu về kiến thức của giáo viên",
        required: true
      }]
    }, {
      key: "s5",
      stt: 5,
      name: "Kết quả nuôi dưỡng, chăm sóc và giáo dục trẻ",
      criteria: [{
        key: "c51",
        stt: 1,
        name: "Năng lực của hiệu trưởng, phó hiệu trưởng trong quá trình triển khai các hoạt động nuôi dưỡng, chăm sóc và giáo dục trẻ",
        required: true
      }, {
        key: "c52",
        stt: 2,
        name: "Số lượng, trình độ đào tạo và yêu cầu về kiến thức của giáo viên",
        required: true
      }]
    }];

    this.setState({
      standards
    });
  }

  renderCriteria = (standardKey, criteria) => (
    <div>
      {criteria.map(criterion => {
        let color = criterion.required? "red" : "blue";
        if (this.state.showCriteria !== undefined && this.state.showCriteria.criterion.key === criterion.key)
          color = "green";
        return (
          <Popup 
            key={criterion.key} 
            trigger={
              <Label 
                as="a" 
                circular 
                size="small" 
                color={color}
                onClick={() => this.setState({ showCriteria: {
                  standardKey,
                  criterion 
                } })}
              >{criterion.stt}
              </Label>
            } 
            content={criterion.name} 
          />
        );
      })}
    </div>
  )

  render() {
    return (
      <div className="resTable">
        <Table unstackable celled striped selectable color="blue">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell colSpan="2">Các tiêu chuẩn đánh giá</Table.HeaderCell>
              <Table.HeaderCell>Tiêu chí</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.state.standards.map(standard => (
              <Table.Row key={standard.key} positive={this.state.showCriteria !== undefined && this.state.showCriteria.standard === standard.key}>
                <Table.Cell collapsing>{standard.stt}</Table.Cell>
                <Table.Cell collapsing>{standard.name}</Table.Cell>
                <Table.Cell>{this.renderCriteria(standard.key, standard.criteria)}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        {(this.state.showCriteria !== undefined)? 
          <Criterion 
            indices={this.state.showCriteria.criterion.indices}
          />
        : null}
      </div>
    );
  }
}