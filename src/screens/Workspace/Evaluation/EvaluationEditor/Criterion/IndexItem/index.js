import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Message, List, Button, Icon } from "semantic-ui-react";
import CKEditor from "react-ckeditor-component";
import { CKEDITOR } from "../../../../../../libs/config";


export default class IndexItem extends Component {

  static propTypes = {
    content: PropTypes.string,
    requirement: PropTypes.string,
    help: PropTypes.string
  };

  static defaultProps = {
    content: "",
    requirement: "",
    help: "",
  }

  constructor(props) {
    super(props);
    
    this.state = {
      isWaiting: false,
    };
  }
  
  renderCircularContent = () => (
    <Message info>
      <Message.Header>Nội dung</Message.Header>
      <List bulleted>
        {this.props.content.map(contentItem => <List.Item>{contentItem}</List.Item>)}
      </List>
    </Message>
  )

  renderCircularRequirement = () => (
    <Message info>
      <Message.Header>Yêu cầu (nội hàm)</Message.Header>
      <List bulleted>
        {this.props.requirement.map(requirementItem => <List.Item>{requirementItem}</List.Item>)}
      </List>
    </Message>
  )

  renderCircularHelp = () => (
    <Message info>
      <Message.Header>Gợi ý</Message.Header>
      <List bulleted>
        {this.props.help.map(helpItem => <List.Item>{helpItem}</List.Item>)}
      </List>
    </Message>
  )

  render() {
    return (
      <Grid columns={2} divided>
        <Grid.Row>
          <Grid.Column>
            <Message info>Mô tả hiện trạng <Button style={{ marginTop: -8}} loading={this.state.isWaiting} primary floated="right"><Icon name="save" />Lưu</Button></Message>
            <CKEditor 
              config={{ height: 300 }}
              scriptUrl={CKEDITOR}
              activeClass="p10"
              content="Trường đã có sự phân định rõ ràng về  trách  nhiệm và quyền hạn của tập thể lãnh đạo và cá nhân cán bộ quản lí, giáo viên và nhân viên"
            />
          </Grid.Column>
          <Grid.Column>{this.renderCircularContent()}{this.renderCircularRequirement()}{this.renderCircularHelp()}</Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}