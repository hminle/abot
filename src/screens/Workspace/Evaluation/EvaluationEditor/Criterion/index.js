import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab, Message, Button, Icon } from "semantic-ui-react";
import CKEditor from "react-ckeditor-component";
import { CKEDITOR } from "../../../../../libs/config";

// components
import IndexItem from "./IndexItem";


export default class Criterion extends Component {
  
  static propTypes = {
    indices: PropTypes.arrayOf(PropTypes.object.isRequired)
  };

  static defaultProps = {
    indices: [],
  }

  constructor(props) {
    super(props);
    
    this.state = {
      isWaiting: false,
    };
  }

  renderEvaluation = () => (
    <div>
      <Message info>Đánh giá<Button style={{ marginTop: -8}} loading={this.state.isWaiting} primary floated="right"><Icon name="save" />Lưu</Button></Message>
      <CKEditor 
        config={{ height: 300 }}
        scriptUrl={CKEDITOR}
        activeClass="p10"
        content="Đánh giá"
      />
    </div>)

  renderPlan = () => (
    <div>
      <Message info>Kế hoạch cải tiến<Button style={{ marginTop: -8}} loading={this.state.isWaiting} primary floated="right"><Icon name="save" />Lưu</Button></Message>
      <CKEditor 
        config={{ height: 300 }}
        scriptUrl={CKEDITOR}
        activeClass="p10"
        content="Kế hoạch cải tiến"
      />
    </div>)

  render() {
    const panes = [];
    this.props.indices.forEach(index => (
      panes.push({
        menuItem: `${index.name}`, 
        render: () => (
          <Tab.Pane>
            <IndexItem 
              content={index.content}
              requirement={index.requirement}
              help={index.help}
            />
          </Tab.Pane>)
      })
    ))
    panes.push({ menuItem: "Đánh giá", render: () => <Tab.Pane>{this.renderEvaluation()}</Tab.Pane> });
    panes.push({ menuItem: "Kế hoạch cải tiến", render: () => <Tab.Pane>{this.renderPlan()}</Tab.Pane> });
    
    return (
      <Tab
        color="blue" 
        menu={{ color: "blue", secondary: true, pointing: true }}
        panes={panes} 
      />
    );
  }
}