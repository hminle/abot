import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Grid, Message, Button, Icon, Dimmer, Loader } from "semantic-ui-react";
import CKEditor from "react-ckeditor-component";
import { CKEDITOR, ACTIVE_YEAR } from "../../../../libs/config";

import { db } from "../../../../libs/firebase";


class Description extends Component {
  static propTypes = {
    unitID: PropTypes.string.isRequired,
    errorHandler: PropTypes.func.isRequired,
    successHandler: PropTypes.func.isRequired,
    description: PropTypes.shape({
      intro: PropTypes.string.isRequired,
      conclusion: PropTypes.string.isRequired,
    }).isRequired
  };

  constructor(props) {
    super(props);
    
    this.state = {
      isWaiting: false,
      intro: '',
      conclusion: ''
    }
  }

  componentWillMount() {
    this.setState({ ...this.props.description })
  }

  handleChange = (key, value) => {
    this.setState(() => ({
      [key]: value
    }))
  }

  saveForm = key => {
    const { unitID } = this.props;
    const path = `description.${key}`
    const value = this.state[key]
    db.collection("evaluation").doc(unitID).collection("years").doc(`${ACTIVE_YEAR}`).update({
      [path]: value
    }).then(() => {
      this.props.successHandler('Đã cập nhật thông tin')
    }).catch(() => {
      this.props.errorHandler('Cập nhật thông tin không thành công')
    })
  }

  render() {
    return (
      <div>
        {this.state.isWaiting?
          <Dimmer active>
            <Loader>Đang kết nối...</Loader>
          </Dimmer> : null
        }
        <Grid columns={2} divided>
          <Grid.Row>
            <Grid.Column>
              <Message info>Đặt vấn đề
                <Button style={{ marginTop: -8}} loading={this.state.isWaiting} primary floated="right" onClick={() => this.saveForm('intro')}><Icon name="save" />Lưu</Button>
              </Message>
              <CKEditor 
                config={{ height: 500 }}
                scriptUrl={CKEDITOR}
                activeClass="p10"
                content={this.state.intro}
                events={{
                  "change": event => this.handleChange('intro', event.editor.getData())
                }}
              />
            </Grid.Column>
            <Grid.Column>
              <Message info>Kết luận
                <Button style={{ marginTop: -8}} loading={this.state.isWaiting} primary floated="right" onClick={() => this.saveForm('conclusion')}><Icon name="save" />Lưu</Button>
              </Message>
              <CKEditor 
                config={{ height: 500 }}
                scriptUrl={CKEDITOR}
                activeClass="p10"
                content={this.state.conclusion}
                events={{
                  "change": event => this.handleChange('conclusion', event.editor.getData())
                }}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  unitID: state.user.unit.unitID,
  description: state.evaluation[ACTIVE_YEAR].description
});

export default connect(mapStateToProps)(Description)