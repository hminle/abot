import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab } from "semantic-ui-react";
import { connect } from "react-redux";
import NotificationSystem from "react-notification-system";

// Components
import EvaluationEditor from "./EvaluationEditor";
import Staffs from "./Staffs";
import Description from "./Description";
import Proof from "./Proof";

// Services
import { ACTIVE_YEAR } from '../../../libs/config';


class Unit extends Component {
  static propTypes = {
    user: PropTypes.shape({
      uid: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      staff: PropTypes.object.isRequired,
      email: PropTypes.string.isRequired,
      gender: PropTypes.string.isRequired,
      unit: PropTypes.shape({
        unitID: PropTypes.string.isRequired,
        information: PropTypes.shape({
          type: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
          address: PropTypes.string.isRequired,
          phone: PropTypes.string,
          province: PropTypes.string,
          district: PropTypes.string,
          ward: PropTypes.string
        }).isRequired,
      }).isRequired
    }).isRequired
  };

  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  render() {
    const resultHandlers = {
      successHandler: this.successHandler,
      errorHandler: this.errorHandler
    };

    const panes = [
      { menuItem: "Thành viên", render: () => (
        <Tab.Pane>
          <Staffs
            {...resultHandlers}
            staffs={this.props.staffs}
            groups={this.props.groups}
          />
        </Tab.Pane>
      )},
      { menuItem: "Minh chứng", render: () => <Tab.Pane><Proof {...resultHandlers} /></Tab.Pane> },
      { menuItem: "Đánh giá", render: () => <Tab.Pane><EvaluationEditor {...resultHandlers} /></Tab.Pane> },
      { menuItem: "Đặt vấn đề, kết luận", render: () => <Tab.Pane><Description {...resultHandlers} /></Tab.Pane> }
    ];
    return (
      <div>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
        <Tab
          color="blue" 
          menu={{ color: "teal", fluid: true, pointing: true, borderless: true }} 
          panes={panes} 
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  staffs: state.evaluation[ACTIVE_YEAR].staffs,
  groups: state.evaluation[ACTIVE_YEAR].groups,
});
export default connect(mapStateToProps)(Unit);