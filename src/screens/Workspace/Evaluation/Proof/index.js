import React, { Component } from "react";
import { Table, Search, Button, Icon } from "semantic-ui-react";


export default class Proof extends Component {
  renderDownloadButton = () => (
    <Button animated>
      <Button.Content visible>Tải về</Button.Content>
      <Button.Content hidden>
        <Icon name="download" />
      </Button.Content>
    </Button>
  )

  render() {
    return (
      <div className="resTable">
        <Search
          placeholder="Gõ mã hoặc tên hình minh chứng để tìm kiếm"
        />
        <Table unstackable celled striped selectable color="blue">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Mã</Table.HeaderCell>
              <Table.HeaderCell>Tên</Table.HeaderCell>
              <Table.HeaderCell>Số, ngày ban hành</Table.HeaderCell>
              <Table.HeaderCell>Cơ quan ban hành</Table.HeaderCell>
              <Table.HeaderCell />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell collapsing><Icon name="folder" /> H1.1.01.01</Table.Cell>
              <Table.Cell>Quyết định bổ nhiệm Hiệu trưởng, Phó hiệu trưởng</Table.Cell>
              <Table.Cell>Số 3932/QĐ-UBND, ngày 30 tháng 11 năm 2017</Table.Cell>
              <Table.Cell>Chủ tịch UBND Huyện</Table.Cell>
              <Table.HeaderCell>{this.renderDownloadButton()}</Table.HeaderCell>
            </Table.Row>
            <Table.Row>
              <Table.Cell collapsing><Icon name="folder" /> H1.1.01.05</Table.Cell>
              <Table.Cell>Quyết định thành lập hội đồng chấm sáng kiến kinh nghiệm</Table.Cell>
              <Table.Cell>Số 01/QĐ-MGHS, ngày 01/12/2017</Table.Cell>
              <Table.Cell>PHT trường MGHS</Table.Cell>
              <Table.HeaderCell>{this.renderDownloadButton()}</Table.HeaderCell>
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    );
  }
}