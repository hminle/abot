import React, { Component } from "react";
import PropTypes from "prop-types";
import { Table, Dropdown, Button, Icon, Modal, Form, Input, Dimmer, Loader, Grid, Sidebar, Segment } from "semantic-ui-react";
import _ from "lodash";
import { connect } from "react-redux";
import uuid from 'uuid/v4';

import { db } from "../../../../libs/firebase";
import { ACTIVE_YEAR } from '../../../../libs/config';

const formnames = {
  staff: "staffForm",
  group: "groupForm"
}

class Staffs extends Component {
  static propTypes = {
    unitID: PropTypes.string.isRequired,
    groups: PropTypes.object,     // eslint-disable-line react/forbid-prop-types
    staffs: PropTypes.object,     // eslint-disable-line react/forbid-prop-types
    setting: PropTypes.shape({
      positions: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
      })).isRequired,
      roles: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
      })).isRequired
    }).isRequired,
    errorHandler: PropTypes.func.isRequired,
    successHandler: PropTypes.func.isRequired,
  };

  static defaultProps = {
    groups: {},
    staffs: {}
  }

  constructor(props) {
    super(props);

    this.state = {
      isWaiting: false,
      staffs: this.props.staffs,
      groups: this.props.groups,
      dropdownPositions: this.getPROptions(this.props.setting.positions),
      dropdownRoles: this.getPROptions(this.props.setting.roles),
      dropdownGroups: this.getGroupOptions(this.props.groups),
      staffForm: {},
      groupForm: {},
      staffFormActive: false,
      groupFormActive: false,
      showSidebar: false
    }
  }

  //
  // MARK  helper functions
  //

  getPROptions = (arr) => 
    _.map(arr, item => ({ 
      key: item.key,
      value: item.key,
      text: item.text
    }))

  getGroupOptions = (groups) => 
    _.map(_.keys(groups), (id) => ({
      key: id,
      value: id,
      text: groups[id].name
    }))

  //
  // MARK  common handle form
  //

  showDetailForm = (e, {formname, fields}) => {
    let formFields = fields;
    if (fields === undefined) {
      if (formname === formnames.staff)
        formFields = {
          email: '',
          order: '',
          displayName: '',
          position: '',
          role: '',
          group: []
        }
      else if (formname === formnames.group)
        formFields = {
          key: uuid(),
          order: '',
          name: '',
          lead: ''
        }
    }
    this.setState({
      [`${formname}Active`]: true,
      [formname]: { 
        ...this.state[formname],
        ...formFields
      }
    });
  }

  updateDetailForm = (e, {formname, name, value}) => {
    this.setState({ 
      [formname]: { 
        ...this.state[formname],
        [name]: value
      }
    });
  }

  closeDetailForm = (e, {formname}) => {
    this.setState({
      [`${formname}Active`]: false
    });
  }

  //
  // MARK  handle staff form
  //

  validateStaffForm = (formname) => {
    // check null
    const notNullFields = {
      displayName: 'Họ và tên', 
      email: 'địa chỉ email',
      position: 'chức danh',
      role: 'nhiệm vụ',
    };
    
    const result = _.compact(_.map(notNullFields, (label, key) => {
      const value = this.state[formname][key];
      return !value ? label : false;
    }))

    if (_.size(result) !== 0 ) {
      this.props.errorHandler(`Bạn chưa nhập ${result.pop()}`)
      return false;
    }

    // check number
    const numberFields = {order: 'số thứ tự'}
    const result2 = _.compact(_.map(numberFields, (label, key) => {
      const value = this.state[formname][key];
      const isNumber = /^[0-9]+$/.test(value);
      return !isNumber ? label : false;
    }))
    
    if (_.size(result2) !== 0 ) {
      this.props.errorHandler(`${result2.pop()} phải nhập số.`)
      return false
    }

    return true;
  }

  finishStaffForm = () => {
    this.setState({ 
      staffFormActive: false,
      staffs: {
        ...this.state.staffs,
        [this.state.staffForm.email]: { ...this.state.staffForm }
      }
    });
  }

  //
  // MARK  handle group form
  //

  finishGroupForm = () => {
    this.setState({ 
      groupFormActive: false,
      groups: {
        ...this.state.groups,
        [this.state.groupForm.key]: { ...this.state.groupForm }
      }
    });
  }

  //
  // MARK  save data to firebase
  //

  handleSave = () => {
    this.setState({ isWaiting: true });
    db.collection("evaluation").doc(this.props.unitID).collection("years").doc(`${ACTIVE_YEAR}`).set({ 
      staffs: this.state.staffs, 
      groups: this.state.groups 
    }, { mergeFields: ['staffs', 'groups'] }
    ).then(() => {
      this.setState({ isWaiting: false});
      this.props.successHandler(`Đã lưu dữ liệu thành công.`);
    }).catch(() => {
      this.setState({ isWaiting: false});
      this.props.errorHandler(`Có lỗi khi lưu dữ liệu.`)
    })
  }

  //
  // MARK  render sub components
  //

  deleteRow = (e, {formname, rowkey}) => {
    if (formname === formnames.staff) {
      const newStaffs = { ...this.state.staffs };
      delete newStaffs[rowkey];
      this.setState({ staffs: newStaffs });
    } else if (formname === formnames.group) {
      const newGroups = { ...this.state.groups };
      delete newGroups[rowkey];
      this.setState({ groups: newGroups });
    } 
  }

  renderStaffForm = () => {
    const { email, order, displayName, position, role, groups } = this.state.staffForm;
    return (
      <Modal formname="staffForm" size="small" open={this.state.staffFormActive} onClose={this.closeDetailForm}>
        <Modal.Header>Thông tin thành viên</Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Field required>
              <label>Số thứ tự</label>
              <Input placeholder="Nhập số thứ tự" formname="staffForm" name="order" value={order} onChange={this.updateDetailForm} />
            </Form.Field>
            <Form.Field required>
              <label>Họ và tên</label>
              <Input placeholder="Nhập họ và tên" formname="staffForm" name="displayName" value={displayName} onChange={this.updateDetailForm} />
            </Form.Field>
            <Form.Field required>
              <label>Email</label>
              <Input placeholder="Nhập địa chỉ email" formname="staffForm" name="email" value={email} onChange={this.updateDetailForm} />
            </Form.Field>
            <Form.Field>
              <label>Chức danh</label>
              <Dropdown 
                formname="staffForm" 
                name="position"
                placeholder="Chọn chức danh"
                search
                selection 
                multiple={false} 
                options={this.state.dropdownPositions} 
                value={position} 
                onChange={this.updateDetailForm}
              />
            </Form.Field>
            <Form.Field>
              <label>Nhiệm vụ</label>
              <Dropdown
                formname="staffForm"  
                name="role"
                placeholder="Chọn nhiệm vụ"
                search
                selection 
                options={this.state.dropdownRoles} 
                value={role} 
                onChange={this.updateDetailForm}
              />
            </Form.Field>
            <Form.Field>
              <label>Nhóm</label>
              <Dropdown
                formname="staffForm"  
                name="groups"
                placeholder="Chọn nhóm"
                search
                selection 
                multiple 
                options={this.state.dropdownGroups} 
                value={groups} 
                onChange={this.updateDetailForm}
              />
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button positive onClick={this.finishStaffForm}>Hoàn tất</Button>
          <Button negative formname="staffForm" onClick={this.closeDetailForm}>Đóng</Button>
        </Modal.Actions>
      </Modal>
    )
  }
  
  renderGroupForm = () => {
    const { key, order, name, lead } = this.state.groupForm;
    const dropdownEmails = _.map(_.keys(this.state.staffs), (email) => ({
      key: email,
      text: email,
      value: email
    }));
    return (
      <Modal size="small" formname="groupForm" open={this.state.groupFormActive} onClose={this.closeDetailForm}>
        <Modal.Header>Thông tin nhóm</Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Field required>
              <label>Số thứ tự</label>
              <Input placeholder="Nhập số thứ tự" name="order" value={order} formname="groupForm" onChange={this.updateDetailForm} />
            </Form.Field>
            <Form.Field required>
              <label>Tên nhóm</label>
              <Input placeholder="Nhập tên nhóm" name="name" value={name} formname="groupForm" onChange={this.updateDetailForm} />
            </Form.Field>
            <Form.Field>
              <label>Trưởng nhóm</label>
              <Dropdown 
                name="lead"
                formname="groupForm"
                placeholder="Chọn trưởng nhóm"
                search
                selection
                options={dropdownEmails} 
                value={lead} 
                onChange={this.updateDetailForm}
              />
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button positive rowkey={key} onClick={this.finishGroupForm}>Hoàn tất</Button>
          <Button negative formname="groupForm" onClick={this.closeDetailForm}>Đóng</Button>
        </Modal.Actions>
      </Modal>
    )
  }

  renderGroup = () => (
    <Segment>
      <Table unstackable celled striped selectable color="blue">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>STT</Table.HeaderCell>
            <Table.HeaderCell>Tên nhóm</Table.HeaderCell>
            <Table.HeaderCell>Trưởng nhóm</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {_.map(_.orderBy(this.state.groups, ['order'], ['asc']), group => (
            <Table.Row key={group.key}>
              <Table.Cell collapsing>{group.order}</Table.Cell>
              <Table.Cell>{group.name}</Table.Cell>
              <Table.Cell>{group.lead}</Table.Cell>
              <Table.Cell collapsing>
                <Button color='green' icon='edit' formname="groupForm" fields={group} onClick={this.showDetailForm} />
                <Button color='red' icon='trash' formname="groupForm" rowkey={group.key} onClick={this.deleteRow} />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
      <Button color='green' formname={formnames.group} onClick={this.showDetailForm}><Icon name="add" />Thêm nhóm</Button>
    </Segment>
  )

  renderStaff = () => (
    <Segment>
      <Table styles={{ height: "100%" }} unstackable celled striped selectable color="blue">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>STT</Table.HeaderCell>
            <Table.HeaderCell>Họ và tên</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Chức vụ</Table.HeaderCell>
            <Table.HeaderCell>Nhiệm vụ</Table.HeaderCell>
            <Table.HeaderCell>Nhóm</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {_.map(_.orderBy(this.state.staffs, ['order'], ['asc']), staff => {
            const position = _.takeWhile(this.props.setting.positions, { key: staff.position });
            const role = _.takeWhile(this.props.setting.roles, { key: staff.role });
            return (
              <Table.Row key={staff.email}>
                <Table.Cell collapsing>{staff.order}</Table.Cell>
                <Table.Cell>{staff.displayName}</Table.Cell>
                <Table.Cell>{staff.email}</Table.Cell>
                <Table.Cell>{position.length > 0 ? position[0].text : ''}</Table.Cell>
                <Table.Cell>{role.length > 0 ? role[0].text : ''}</Table.Cell>
                <Table.Cell>{_.join(_.map(staff.groups, groupKey => this.props.groups[groupKey].name), ', ')}</Table.Cell>
                <Table.Cell collapsing>
                  <Button color='green' icon='edit' formname="staffForm" fields={staff} onClick={this.showDetailForm} />
                  <Button color='red' icon='trash' formname="staffForm" rowkey={staff.email} onClick={this.deleteRow} />
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <Button color='green' formname={formnames.staff} onClick={this.showDetailForm}><Icon name="add" />Thêm thành viên</Button>
    </Segment>
  )

  render() {
    return (
      <div>
        {this.state.isWaiting?
          <Dimmer active>
            <Loader>Đang lưu dữ liệu ...</Loader>
          </Dimmer> : null
        }
        {this.renderStaffForm()}
        {this.renderGroupForm()}
        <Button style={{ marginLeft: 15, marginBottom: 15 }} loading={this.state.isWaiting} primary onClick={this.handleSave}><Icon name="save" />Lưu</Button>
        <Button style={{ marginBottom: 15 }} floated="right" color="brown" onClick={() => this.setState({ showSidebar: !this.state.showSidebar })}><Icon name="save" />Quản lý nhóm</Button>
        <Sidebar.Pushable>
          <Sidebar
            direction="right"
            animation='push'
            onHide={() => this.setState({ showSidebar: false })}
            visible={this.state.showSidebar}
            width="very wide"
          >
            {this.renderGroup()}
          </Sidebar>
          <Sidebar.Pusher>
            {this.renderStaff()}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
        {/* <Grid columns={2} stackable divided>
          <Grid.Row>
            <Button style={{ marginLeft: 15 }} loading={this.state.isWaiting} primary onClick={this.handleSave}><Icon name="save" />Lưu</Button>
            <Button primary onClick={() => this.setState({ showSidebar: !this.state.showSidebar })}><Icon name="save" />Quản lý nhóm</Button>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {this.renderStaff()}
            </Grid.Column>
            <Grid.Column>
              {this.renderGroup()}
            </Grid.Column>
          </Grid.Row>
        </Grid> */}
      </div>
    );
  }
}

//
// MARK  connect to store
//

const mapStateToProps = (state) => ({
  unitID: state.user.unit.unitID,
  setting: state.setting,
});

export default connect(mapStateToProps)(Staffs);