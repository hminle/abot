import React, { Component } from "react";
import PropTypes from "prop-types";
import { Menu, Icon, Modal, Header, Message, Dimmer, Loader, Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import axios from "axios";

// components
import ProfileForm from "../../components/ProfileForm";
import UserHeader from "../../components/UserHeader";
import Unit from "./Unit";
import UnitsSelector from "./UnitsSelector";
import Evaluation from "./Evaluation";
import Documents from "./Documents";
import Settings from "./Settings";

// services
import { RESOURCES, ACTIVE_YEAR } from "../../libs/config";
import { exception } from "../../libs/ga";
import { db } from "../../libs/firebase";
import { fetchSetting } from "../../redux/actions/settings";
import { fetchEvaluation } from '../../redux/actions/evaluation';


class Workspace extends Component {
  static propTypes = {
    fetchSetting: PropTypes.func.isRequired,
    fetchEvaluation: PropTypes.func.isRequired,
    user: PropTypes.shape({
      uid: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      gender: PropTypes.string.isRequired,
      staff: PropTypes.object.isRequired,
      unit: PropTypes.shape({
        unitID: PropTypes.string,
        information: PropTypes.object
      }).isRequired
    }).isRequired,
  };

  constructor(props) {
    super(props);
    
    this.state = {
      activeMenu: "unit",
      isWaiting: false,
      error: ""
    }
  }

  componentWillMount() {
    const { unitID } = this.props.user.unit;
    if (unitID === undefined) {
      this.setState({ showUnitsSelector: true });
      return;
    }

    if (this.props.user.gender === undefined) {
      this.setState({
        profile: {
          forceUpdate: true,
          messages: ["Xin hãy cập nhật thông tin về giới tính để giúp hệ thống tương tác với thầy/cô tốt hơn."]
        }
      });
    }

    this.setState({ isWaiting: true });
    this.evaluationListener = db.collection("evaluation").doc(unitID).onSnapshot(data => {
      if (data.exists) {
        let positions = false;
        let roles = false;
        if (data.data().catalogs !== undefined && data.data().catalogs.positions !== undefined) 
          this.props.fetchSetting("positions", data.data().catalogs.positions);
        else
          positions = true;
        if (data.data().catalogs !== undefined && data.data().catalogs.roles !== undefined)
          this.props.fetchSetting("roles", data.data().catalogs.roles);
        else
          roles = true;
        if (positions || roles)
          this.getDefaultCatalogs(positions, roles);
        else
          this.setState({ isWaiting: false });
      } else
        this.getDefaultCatalogs();
    }, error => {
      exception(error);
      this.setState({ isWaiting: false, error: "Kết nối đến hệ thống danh mục không thành công." });
    });
    
    this.evaluationYearListener = db.collection("evaluation").doc(unitID).collection("years").doc(`${ACTIVE_YEAR}`).onSnapshot(data => {
      if (data.exists) {
        this.props.fetchEvaluation(ACTIVE_YEAR, data.data());
      }
    }, error => {
      exception(error);
      this.setState({ isWaiting: false, error: "Kết nối đến hệ thống danh mục không thành công." });
    })
  }

  componentWillUnmount() {
    this.evaluationListener();
    this.evaluationYearListener();
  }

  getDefaultCatalogs = (positions, roles) => {
    axios.get(`${RESOURCES}/abot/catalogs.json`).then(catalogs => {
      if (catalogs.status === 200 && catalogs.data !== undefined) {
        if (positions)
          this.props.fetchSetting("positions", catalogs.data.positions);
        if (roles)
          this.props.fetchSetting("roles", catalogs.data.roles);
        this.setState({ isWaiting: false });
      } else
        this.setState({ isWaiting: false, error: "Kết nối đến hệ thống danh mục không thành công." })
    }).catch((error) => {
      exception(error);
      this.setState({ isWaiting: false, error: "Kết nối đến hệ thống danh mục không thành công." });
    });
  }

  renderProfileForm = () => {
    if (this.state.profile === undefined)
      return null;
    return (
      <Modal dimmer={false} size="tiny" open>
        <Header className="form-header" as="h3" icon="id card outline" content="Thông tin tài khoản" />
        <Modal.Content>
          <ProfileForm 
            closeHandler={() => this.setState({ profile: undefined })}
            forceUpdate={this.state.profile.forceUpdate}
            messages={this.state.profile.messages} 
          />
        </Modal.Content>
      </Modal>
    );
  }

  renderContent = () => {
    if (this.state.activeMenu === "unit")
      return <Unit />;
    if (this.state.activeMenu === "evaluation")
      return <Evaluation />;
    if (this.state.activeMenu === "documents")
      return <Documents />;
    if (this.state.activeMenu === "settings")
      return <Settings />;
    return <Message error>Tính năng này chưa được phát triển</Message>;
  }

  renderMenu = () => (
    <Menu className="smallwrap" style={{ backgroundColor: "#208286", marginBottom: 0 }} inverted secondary pointing icon="labeled">
      <Menu.Item name="unit" active={this.state.activeMenu === "unit"} onClick={() => this.setState({ activeMenu: "unit" })}>
        <Icon name="info" />Đơn vị
      </Menu.Item>
      <Menu.Item name="evaluation" active={this.state.activeMenu === "evaluation"} onClick={() => this.setState({ activeMenu: "evaluation" })}>
        <Icon name="tasks" />Tự đánh giá
      </Menu.Item>
      <Menu.Item name="documents" active={this.state.activeMenu === "documents"} onClick={() => this.setState({ activeMenu: "documents" })}>
        <Icon name="folder open" />Thư viện văn bản
      </Menu.Item>
      <Menu.Item name="settings" active={this.state.activeMenu === "settings"} onClick={() => this.setState({ activeMenu: "settings" })}>
        <Icon name="settings" />Cấu hình
      </Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item><UserHeader profileHandler={() => this.setState({ profile: true })} /></Menu.Item>
      </Menu.Menu>
    </Menu>
  )

  render() {
    if (this.state.isWaiting)
      return (
        <Dimmer active inverted>
          <Loader inverted style={{minWidth: 300}}>Kết nối danh mục...</Loader>
        </Dimmer> 
      );
    if (this.state.showUnitsSelector)
      return <UnitsSelector  />;
    return (
      <div>
        {this.renderProfileForm()}
        {this.renderMenu()}
        {(this.state.error !== "")?
          <Container textAlign="center">
            <Message
              error
              header={this.state.error}
              list={[
                "Truy cập lại hệ thống ở tab khác hoặc vào thời điểm khác.",
                "Liên hệ đến công ty để được hỗ trợ.",
              ]}
            />
          </Container>
          : this.renderContent()
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  setting: state.setting,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchSetting, fetchEvaluation }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Workspace);