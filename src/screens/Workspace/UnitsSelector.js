import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal, Button, Form, Dropdown, Dimmer, Loader, Container, Segment, Header, Divider, List } from "semantic-ui-react";
import { connect } from "react-redux";
import axios from "axios";
import _ from "lodash";
import NotificationSystem from "react-notification-system";
import { bindActionCreators } from "redux";

// services
import { app, db, sgd } from "../../libs/firebase";
import { RESOURCES } from "../../libs/config";
import { signOut } from "../../redux/actions/users";


const cities = require("../../libs/cities.json");

class Unit extends Component {
  static propTypes = {
    signOut: PropTypes.func.isRequired,
    user: PropTypes.shape({
      email: PropTypes.string.isRequired,
    }).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      isWaiting: true,
      selectedUnit: "",
      getUnitsError: "",
      units: []
    };
  }

  componentWillMount() {
    axios.get(`${RESOURCES}/abot/units/${sgd}.json`).then(data => {
      if (data.status === 200 && data.data !== undefined) {
        this.setState({ 
          isWaiting: false, 
          units: _.map(data.data, unit => {
            let district = "";
            let ward = "";
            if (cities[unit.province].districts[unit.district] !== undefined) {
              district = cities[unit.province].districts[unit.district].name;
              if (cities[unit.province].districts[unit.district].wards[unit.ward] !== undefined)
                ward = cities[unit.province].districts[unit.district].wards[unit.ward].name;
            }
            return { key: unit.id, text: `${unit.name}. ${district}. ${ward}`, value: unit.id };
          })
        });
      } else
        this.setState({ isWaiting: false, getUnitsError: data.status });
    }).catch(error => this.setState({ isWaiting: false, getUnitsError: error }));
  }
  
  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  updateUserActiveUnit = () => {
    if (this.state.selectedUnit === "") {
      this.errorHandler("Chưa chọn trường đang công tác.");
      return;
    }
    this.setState({ isWaiting: true });
    db.collection("units").doc(this.state.selectedUnit).get().then(unit => {
      if (unit.exists) {
        db.collection("users").doc(this.props.user.email).set({
          activeUnit: this.state.selectedUnit
        }, { mergeFields: ["activeUnit"] }).then(() => {
          this.setState({ isWaiting: false });
          this.successHandler("Đăng ký thành công.");
          window.location.reload();
        }).catch(() => {
          this.setState({ isWaiting: false });
          this.errorHandler("Cập nhật thông tin đăng ký không thành công. Xin vui lòng thử lại vào lúc khác.");
        });
      } else {
        this.setState({ isWaiting: false });
        this.errorHandler("Không tìm thấy đơn vị đã chọn. Xin vui lòng thử lại vào lúc khác.");
      }
    }).catch(() => {
      this.setState({ isWaiting: false });
      this.errorHandler("Tài khoản chưa được cấp quyền truy cập vô đơn vị đã chọn. Xin vui lòng liên hệ Admin/Hiệu trưởng để được cấp quyền.");
    });
  }

  logoutHandler = () => {
    app.auth().signOut().then(() => {
      this.props.signOut();
    });
  }

  renderError = () => {
    if (this.state.getUnitsError !== "")
      return (
        <Container textAlign="center">
          <Segment stacked>
            <Header as="h3" icon="frown" content="Đã có lỗi xảy ra khi lấy danh sách đơn vị." />
            <Divider />
            <Header as="h4" content="Xin hãy thử các cách sau" />
            <List>
              <List.Item icon="refresh" content="Mở lại trang web ở tab khác hoặc trên trình duyệt khác." />
              <List.Item icon="retweet" content="Đăng xuất ra khỏi hệ thống rồi đăng nhập lại." />
              <List.Item icon="comments outline" content="Liên hệ đến nhân viên hỗ trợ để được tư vấn." />
            </List>
          </Segment>
        </Container>
      );
    return null;
  }

  render() {
    return (
      <Modal className="custom" open>
        <Modal.Header className="form-header">Chọn trường đang công tác</Modal.Header>
        <Modal.Content scrolling>
          {this.state.isWaiting?
            <Dimmer active inverted>
              <Loader inverted>Đang kết nối...</Loader>
            </Dimmer> : null
          }
          {this.renderError()}
          <Form style={{height: 400}}>
            <Form.Field>
              <label>Tên trường: </label>
              <Dropdown 
                search 
                selection 
                multiple={false} 
                onChange={(event, { value }) => this.setState({ selectedUnit: value })} 
                options={this.state.units} 
                value={this.state.selectedUnit} 
                placeholder="Gõ tên trường để tìm kiếm đơn vị"
              />
            </Form.Field>
          </Form>
          <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
        </Modal.Content>
        <Modal.Actions>
          <Button positive onClick={this.updateUserActiveUnit}>Đăng ký sử dụng</Button>
          <Button negative onClick={this.logoutHandler}>Đăng xuất</Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  signOut
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Unit);