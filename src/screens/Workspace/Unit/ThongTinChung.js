import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Input, Checkbox, Icon, Dimmer, Loader, Radio, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import _ from "lodash";
import { db } from "../../../libs/firebase";
// components
import Address from "../../Xbot/Units/address";


const cities = require("../../../libs/cities.json");

class ThongTinChung extends Component {
  static propTypes = {
    successHandler: PropTypes.func.isRequired,
    errorHandler: PropTypes.func.isRequired,
    user: PropTypes.shape({
      unit: PropTypes.shape({
        unitID: PropTypes.string.isRequired,
        information: PropTypes.shape({
          type: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
          oldName: PropTypes.string,
          address: PropTypes.string.isRequired,
          phone: PropTypes.string,
          fax: PropTypes.string,
          website: PropTypes.string,
          province: PropTypes.string,
          district: PropTypes.string,
          ward: PropTypes.string,
          pgd: PropTypes.string,
          sgd: PropTypes.string,
          schoolType: PropTypes.string,
          schoolModel: PropTypes.string.isRequired,
          headmaster: PropTypes.string.isRequired,
          founded: PropTypes.string, 
          branches: PropTypes.string, 
          nationalStandard: PropTypes.string,
          other: PropTypes.shape({
            poorArea: PropTypes.bool,
            crossBorder: PropTypes.bool,
            studentWithDisability: PropTypes.bool,
            daySchool: PropTypes.bool,
            boardingSchool: PropTypes.bool
          })
        }).isRequired,
      }).isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    
    this.state = {
      isWaiting: false,
      unitInfo: this.props.user.unit.information !== undefined ? this.props.user.unit.information : {}
    };
  }

  // helper functions
  updateState = (e, { name, value }) => {
    this.setState({
      unitInfo: {
        ...this.state.unitInfo,
        [name]: value
      }
    });
  }


  validateForm = () => {
    const notNullFields = { name: 'Tên Trường', headmaster: 'Tên Hiệu trưởng' };
    const numberFields = { fax: 'số Fax', phone: 'số điện thoại nhà trường', founded: 'năm thành lập.' };

    // check null
    const result = _.compact(_.map(notNullFields, (label, key) => {
      const value = this.state.unitInfo[key];
      return !value ? label : false;
    }))

    if (_.size(result) !== 0 ) {
      this.props.errorHandler(`Bạn chưa nhập ${result.pop()}`)
      return false;
    }

    // check number
    const result2 = _.compact(_.map(numberFields, (label, key) => {
      const value = this.state.unitInfo[key];
      const isNumber = /^[0-9]+$/.test(value);
      return !isNumber ? label : false;
    }))
    
    if (_.size(result2) !== 0 ) {
      this.props.errorHandler(`${result2.pop()} phải nhập số.`)
      return false
    }
    
    return true;
  }


  // CURD
  saveForm = () => {
    if (this.validateForm()) {
      this.setState({ isWaiting: true })
      db.collection("units").doc(this.props.user.unit.unitID).update(this.state.unitInfo)
      .then(() => {
        this.setState({ isWaiting: false });
        this.props.successHandler('Đã cập nhật thông tin.');
      })
      .catch(() => {
        this.setState({ isWaiting: false })
        this.props.errorHandler('Cập nhật thông tin không thành công.')
      })
    }
  }

  renderParentUnit = () => {
    const { province, district, pgd, sgd } = this.props.user.unit.information; 
    let unitName = "";
    if (pgd !== undefined) {
      if (cities[province] !== undefined && cities[province].districts[district] !== undefined)
        unitName = `Phòng Giáo dục và Đào tạo ${cities[province].districts[district].name}`;
    } else if (sgd !== undefined) {
      if (cities[province] !== undefined)
        unitName = `Sở Giáo dục và Đào tạo ${cities[province].name}`;
    }
    return <Input value={unitName} disabled />
  }

  renderOthers = () => {
    let otherFields = {
      poorArea: "Vùng đặc biệt khó khăn",
      crossBorder: "Trường liên kết với nước ngoài"
    };
    const { schoolType } = this.props.user.unit.information;
    if (schoolType !== undefined && schoolType !== "mn") {
      otherFields = {
        ...otherFields, 
        studentWithDisability: "Trường có học sinh khuyết tật",
        daySchool: "Có học sinh bán trú",
        boardingSchool: "Có học sinh nội trú"
      }
    }
    return _.keys(otherFields).map((otherKey, otherIdx) => (
      <Form.Field key={otherIdx}>
        <Checkbox
          label={otherFields[otherKey]}
          name={otherKey}
          checked={this.state.unitInfo.other[otherKey] === true}
          onChange={(e, { name, checked }) => {
            this.setState({ 
              unitInfo: {
                ...this.state.unitInfo,
                other: {
                  ...this.state.unitInfo.other,
                  [name]: checked
                }
              } 
            });
          }} 
        />
      </Form.Field>
    ));
  }

  render() {
    const { province, district, ward } = this.props.user.unit.information;
    return (
      <div>
        {this.state.isWaiting?
          <Dimmer active>
            <Loader>Đang kết nối...</Loader>
          </Dimmer> : null
        }
        <Form>
          <Button floated="right" loading={this.state.isWaiting} primary onClick={this.saveForm}><Icon name="save" />Lưu</Button>
          <Form.Field required>
            <label>Tên trường (theo QĐ mới nhất)</label>
            <Input required placeholder="Nhập tên trường" name="name" value={this.state.unitInfo.name} onChange={this.updateState} />
          </Form.Field>
          <Form.Field>
            <label>Tên cũ (nếu có)</label>
            <Input placeholder="Nhập tên cũ của trường nếu có" name="oldName" value={this.state.unitInfo.oldName} onChange={this.updateState} />
          </Form.Field>
          <Form.Field control={Input} label="Cơ quan chủ quản">
            {this.renderParentUnit()}
          </Form.Field>
          <Address province={province} district={district} ward={ward} locked />
          <Form.Field required>
            <label>Tên hiệu trưởng</label>
            <Input required placeholder="Nhập tên của hiệu trưởng" name="headmaster" value={this.state.unitInfo.headmaster} onChange={this.updateState} />
          </Form.Field>
          <Form.Group inline>
            <Form.Field>
              <label>Điện thoại trường</label>
              <Input placeholder="Nhập sdt của nhà trường" name="phone" value={this.state.unitInfo.phone} onChange={this.updateState} />
            </Form.Field>
            <Form.Field>
              <label>Số FAX</label>
              <Input placeholder="Nhập số FAX" name="fax" value={this.state.unitInfo.fax} onChange={this.updateState} />
            </Form.Field>
            <Form.Field>
              <label>Website</label>
              <Input placeholder="Nhập website của trường" name="website" value={this.state.unitInfo.website} onChange={this.updateState} />
            </Form.Field>
          </Form.Group>
          <Form.Group inline>
            <Form.Field>
              <label>Năm thành lập</label>
              <Input name="founded" value={this.state.unitInfo.founded} onChange={this.updateState} />
            </Form.Field>
            <Form.Field>
              <label>Số điểm trường</label>
              <Input name="branches" value={this.state.unitInfo.branches} onChange={this.updateState} />
            </Form.Field>
            <Form.Field>
              <label>Đạt chuẩn quốc gia</label>
              <Input name="nationalStandard" value={this.state.unitInfo.nationalStandard} onChange={this.updateState} />
            </Form.Field>
          </Form.Group>
          <Form.Group inline>
            <label>Loại hình trường</label>
            <Form.Field control={Radio} label='Công lập' name='schoolModel' value='cl' checked={this.state.unitInfo.schoolModel === 'cl'} onChange={this.updateState} />
            <Form.Field control={Radio} label='Bán công' name='schoolModel' value='bc' checked={this.state.unitInfo.schoolModel === 'bc'} onChange={this.updateState} />
            <Form.Field control={Radio} label='Dân lập' name='schoolModel' value='dl' checked={this.state.unitInfo.schoolModel === 'dl'} onChange={this.updateState} />
            <Form.Field control={Radio} label='Tư thục' name='schoolModel' value='tt' checked={this.state.unitInfo.schoolModel === 'tt'} onChange={this.updateState} />
          </Form.Group>
          <Form.Group inline>
            <label>Thông tin khác</label>
            {this.renderOthers()}
          </Form.Group>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(ThongTinChung);