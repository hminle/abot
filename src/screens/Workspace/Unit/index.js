import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab } from "semantic-ui-react";
import { connect } from "react-redux";
import NotificationSystem from "react-notification-system";
import _ from 'lodash';

// Components
import ThongTinChung from "./ThongTinChung";
import SchoolInfo from "./SchoolInfo";
import CanBoQuanLy from "./CanBoQuanLy";
import reports from './reports.json';

import { ACTIVE_YEAR } from "../../../libs/config";

class Unit extends Component {
  static propTypes = {
    user: PropTypes.shape({
      unit: PropTypes.shape({
        information: PropTypes.shape({
          schoolType: PropTypes.string,
        }).isRequired,
      }).isRequired
    }).isRequired,
    schoolInfo: PropTypes.shape({
      schoolType: PropTypes.string
    })
  };

  static defaultProps = {
    schoolInfo: {}
  }

  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  render() {
    const { schoolType } = this.props.user.unit.information;
    const { schoolInfo } = this.props;                     
    const years = _.range(ACTIVE_YEAR - 4, ACTIVE_YEAR + 1);    // ex: years = [2014, 2015, 2016, 2017, 2018]

    const resultHandlers = {
      successHandler: this.successHandler,
      errorHandler: this.errorHandler
    };

    const panes = [
      { 
        menuItem: "Thông tin chung", 
        render: () => <Tab.Pane><ThongTinChung {...resultHandlers} /></Tab.Pane> 
      },
      { 
        menuItem: (schoolType === "mn")? "Số nhóm trẻ, lớp mẫu giáo" : "Số lớp", 
        render: () => <Tab.Pane><SchoolInfo {...resultHandlers} name={(schoolType === "mn")? "nhatremaugiao" : "lop"} data={(schoolType === "mn")? schoolInfo.nhatremaugiao : schoolInfo.lop} schema={reports[`lop_${schoolType}`]} years={years} /></Tab.Pane> 
      },
      { 
        menuItem: "Số phòng học", 
        render: () => <Tab.Pane><SchoolInfo {...resultHandlers} name="phonghoc" data={schoolInfo.phonghoc} schema={reports.phonghoc} years={years} /></Tab.Pane> 
      },
      { 
        menuItem: "Cán bộ quản lý", 
        render: () => <Tab.Pane><CanBoQuanLy {...resultHandlers} name="canboquanly" data={schoolInfo.canboquanly} schema={{ hientai: reports.canboquanly_hientai, namhoc: reports.canboquanly_namhoc }} years={years} /></Tab.Pane> 
      },
      { 
        menuItem: (schoolType === "mn")? "Số trẻ" : "Số học sinh",
        render: () => <Tab.Pane><SchoolInfo {...resultHandlers}  name={(schoolType === "mn")? "tre" : "hocsinh"} data={(schoolType === "mn")? schoolInfo.tre : schoolInfo.hocsinh} schema={reports[`hocsinh_${schoolType}`]} years={years} /></Tab.Pane> 
      }
    ];

    return (
      <div>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
        <Tab 
          color="blue" 
          menu={{ color: "teal", fluid: true, pointing: true, borderless: true }} 
          panes={panes} 
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user, evaluation}) => ({
  user,
  schoolInfo: evaluation.schoolInfo,
});

export default connect(mapStateToProps)(Unit);
