import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab } from "semantic-ui-react";

// Components
import SchoolInfo from "./SchoolInfo";


export default class CanBoQuanLy extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    schema: PropTypes.shape({
      hientai: PropTypes.object.isRequired, 
      namhoc: PropTypes.object.isRequired
    }).isRequired,
    years: PropTypes.arrayOf(PropTypes.number).isRequired,
    data: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    successHandler: PropTypes.func.isRequired,
    errorHandler: PropTypes.func.isRequired,
  };

  static defaultProps = {
    data: {}
  };
  
  render() {
    const panes = [{ 
      menuItem: "Hiện tại",
      render: () => (
        <Tab.Pane>
          <SchoolInfo
            {...this.props}
            schema={this.props.schema.hientai}
          />
        </Tab.Pane>
      ) 
    }, {
      menuItem: "Theo Năm học",
      render: () => (
        <Tab.Pane>
          <SchoolInfo
            {...this.props}
            schema={this.props.schema.namhoc} 
          />
        </Tab.Pane> 
      )
    }];

    return (
      <Tab 
        color="blue" 
        menu={{ color: "teal", fluid: true, pointing: true, borderless: true }} 
        panes={panes} 
      />
    );
  }
}