import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { Table, Input, Button, Icon, Dimmer, Loader } from "semantic-ui-react";
import _ from 'lodash';

import { db } from "../../../libs/firebase";
import { ACTIVE_YEAR } from "../../../libs/config";

class SchoolInfo extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    schema: PropTypes.shape({
      rows: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        group: PropTypes.string
      })).isRequired,
      columns: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        group: PropTypes.string
      }))
    }).isRequired,
    years: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    data: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    errorHandler: PropTypes.func.isRequired,
    successHandler: PropTypes.func.isRequired,
    unitID: PropTypes.string.isRequired,
  };

  static defaultProps = {
    data: {}
  };

  constructor(props) {
    super(props);
    
    this.state = {
      isWaiting: false,
      data: this.props.data
    };
  }

  updateState = (e, {path, value}) => {
    const isNumber = /^[0-9]+$/.test(value);
    if (value !== "" && !isNumber) {
      return this.props.errorHandler(`Chỉ được phép nhập số: ${value} không phải là số.`)
    };
    const newData = { ...this.state.data };
    _.setWith(newData, path, value, Object);
    return this.setState({ data: newData });
  }

  saveForm = () => {
    const { unitID, name } = this.props;
    this.setState({ isWaiting: true })
    db.collection("evaluation").doc(unitID).collection("years").doc(`${ACTIVE_YEAR}`).update({
      [`schoolInfo.${name}`]: this.state.data
    }).then( () => {
        this.setState({ isWaiting: false });
        this.props.successHandler('Đã cập nhật thông tin.');
    }).catch(() => {
      this.setState({ isWaiting: false });
      this.props.errorHandler('Cập nhật thông tin không thành công.');
    })
  }

  renderHeader = (columns, years) => {
    if (columns === undefined)
      return (
        <React.Fragment>
          <Table.HeaderCell>Năm học</Table.HeaderCell>
          {years.map(year => <Table.HeaderCell key={year}>{year} - {year+1}</Table.HeaderCell>)}
        </React.Fragment>
      );
    return (
      <React.Fragment>
        <Table.HeaderCell />
        {columns.map(column => <Table.HeaderCell key={column.key}>{column.name}</Table.HeaderCell>)}
      </React.Fragment>
    );
  }

  renderRow = (row, columns, years) => (
    <Table.Row key={row.key}>
      <Table.Cell collapsing>{row.name}</Table.Cell>
      {
        (columns !== undefined) ? 
          _.map(columns, column => {
            const path = `${row.key}.${column.key}`;
            const value = _.get(this.state.data, path, 0);
            return (
              <Table.Cell key={`${path}`}>
                <Input style={{width: 100}} path={path} value={value} onChange={this.updateState} />
              </Table.Cell>
            )
          })
        : _.map(years, year => {
            const path = `${row.key}.${year}`;
            const value = _.get(this.state.data, path, 0);
            return (
              <Table.Cell key={`${path}`}>
                <Input style={{width: 100}} path={path} value={value} onChange={this.updateState} />
              </Table.Cell>
            )
          })
      }
    </Table.Row>
  )

  renderFooter = (rows, columns, years) => {
    let cells = [];
    if (columns !== undefined) {
      cells = _.map(columns, (column) => {
        const total = _.sum(_.map(rows, (row) => _.get(this.state.data, `${row.key}.${column.key}`,0) ));
        return <Table.HeaderCell key={`footer-${column.key}`}>{total}</Table.HeaderCell>;
      })
    } else {
      cells = _.map(years, (year) => {
        const total = _.sum(_.map(rows, (row) => _.get(this.state.data, `${row.key}.${year}`,0) ));
        return <Table.HeaderCell key={`footer-${year}`}>{total}</Table.HeaderCell>
      })
    }
    return cells;
  }

  render() {
    const { schema, years } = this.props;
    return (
      <div className="resTable">
        {this.state.isWaiting?
          <Dimmer active>
            <Loader>Đang kết nối...</Loader>
          </Dimmer> : null
        }
        <Button style={{marginBottom: 5}} loading={this.state.isWaiting} primary floated="right" onClick={this.saveForm}><Icon name="save" />Lưu</Button>
        <Table unstackable celled striped selectable color="blue">
          <Table.Header>
            <Table.Row>
              {this.renderHeader(schema.columns, years)}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {schema.rows.map(row => this.renderRow(row, schema.columns, years))}
          </Table.Body>
      
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell>Tổng cộng</Table.HeaderCell>
              {this.renderFooter(schema.rows, schema.columns, years)}
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  unitID: state.user.unit.unitID
});

export default connect(mapStateToProps)(SchoolInfo)
