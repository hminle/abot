import React, { Component } from "react";
import PropTypes from "prop-types";
import { Menu, Table, Grid, Dimmer, Loader, Button, Modal, Header, Icon, Form } from "semantic-ui-react";
import axios from "axios";
import _ from "lodash";
import { connect } from "react-redux";

import { RESOURCES } from "../../../libs/config";
import { exception } from "../../../libs/ga";
import { db } from "../../../libs/firebase";


const uuid = require("uuid/v1");

const names = {
  position: 'Chức danh',
  role: 'Nhiệm vụ'
};

const actions = {
  add: 'Thêm',
  edit: 'Sửa'
}

class Catalogs extends Component {
  static propTypes = {
    positions: PropTypes.arrayOf(PropTypes.shape({
      key: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired
    })).isRequired,
    roles: PropTypes.arrayOf(PropTypes.shape({
      key: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired
    })).isRequired,
    errorHandler: PropTypes.func.isRequired,
    successHandler: PropTypes.func.isRequired,
    unitID: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isWaiting: false,
      positions: this.props.positions,
      roles: this.props.roles,
      selectedRow: {}
    };
  }

  getDefaultCatalogs = (name) => {
    axios.get(`${RESOURCES}/abot/catalogs.json`).then(catalogs => {
      if (catalogs.status === 200 && catalogs.data !== undefined)
        if (name === names.position)
          this.setState({ positions: catalogs.data.positions });
        else if (name === names.role)
          this.setState({ roles: catalogs.data.roles });
      this.setState({ isWaiting: false })
    }).catch((error) => {
      exception(error);
      this.setState({ isWaiting: false });
    });
  }

  handleDeleteRow = (item) => {
    this.setState({ 
      positions: this.state.positions.filter(elem => elem.key !== item.key), 
      roles: this.state.roles.filter(elem => elem.key !== item.key) 
    });
  }

  handleSave = () => {
    this.setState({ isWaiting: true })
    db.collection("evaluation").doc(this.props.unitID).update({
      catalogs: { positions: this.state.positions, roles: this.state.roles }
    }).then(() => {
      this.setState({ isWaiting: false });
      this.props.successHandler('Đã cập nhật thông tin.');
    }).catch(() => {
      this.setState({ isWaiting: false });
      this.props.errorHandler('Cập nhật thông tin không thành công.');
    })
  }

  handleCloseDetailForm = () => {
    this.setState({
      selectedRow: {
        ...this.state.selectedRow,
        active: false
      }
    });
  }

  handleOpenDetailForm = (name, action, item) => {
    this.setState({
      selectedRow: {
        ...this.state.selectedRow,
        active: true,
        name,
        action,
        key: _.get(item, "key", uuid()),
        text: _.get(item, "text", '')
      }
    })
  }

  validateForm = (text, arr) => {
    if (text.length === 0) {
      this.props.errorHandler('Danh mục không được bỏ trống');
      return false;
    }
    if (arr.some(elem => elem.text === text)) {
      this.props.errorHandler('Danh mục đã tồn tại');
      return false;
    }
    return true;
  }

  handleFinishDetailForm = () => {
    const { name, key, text } = this.state.selectedRow;
    if (name === names.position && this.validateForm(text, this.state.positions)) {
      if (this.state.selectedRow.action === actions.add) {
        this.setState({
          positions: this.state.positions.concat([{ key: uuid(), text }])
        });
      } else if (this.state.selectedRow.action === actions.edit) {
        const editedPositions = [...this.state.positions];
        const index = editedPositions.findIndex(x => x.key === key);
        editedPositions[index] = { key, text };
        this.setState({ positions: editedPositions });
      }
    } else if (name === names.role && this.validateForm(text, this.state.roles)) {
      if (this.state.selectedRow.action === actions.add) {
        this.setState({
          roles: this.state.roles.concat([{ key: uuid(), text }])
        });
      } else if (this.state.selectedRow.action === actions.edit) {
        const editedRoles = [...this.state.roles];
        const index = editedRoles.findIndex(x => x.key === key);
        editedRoles[index] = { key, text };
        this.setState({ roles: editedRoles });  
      }
    }
    this.handleCloseDetailForm();
  }

  handleChange = (e, { value }) => {
    this.setState({
      selectedRow: {
        ...this.state.selectedRow,
        text: value
      }
    })
  }

  renderDetailForm = () => {
    const { active, name, text } = this.state.selectedRow;
    const header = `Nhập ${name}`;
    return (
      <Modal open={active} closeIcon onClose={this.handleCloseDetailForm} >
        <Modal.Header>{header} </Modal.Header>
        <Modal.Content image>
          <Modal.Description>
            <Header>{name}</Header>
            <Form>
              <Form.Group>
                <Form.Input
                  placeholder={`Nhập ${name}`}
                  value={text}
                  onChange={this.handleChange}
                />
              </Form.Group>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.handleFinishDetailForm}>
            <Icon name='checkmark' />Hoàn tất
          </Button>
          <Button color='red' onClick={this.handleCloseDetailForm}>
            <Icon name='close' />Đóng
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }

  renderCatalog = (name, data) => (
    <div>
      <Table unstackable color="blue" celled striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>STT</Table.HeaderCell>
            <Table.HeaderCell colSpan={2}>{name}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {data.map((item, index) => (
            <Table.Row key={item.key}>
              <Table.Cell collapsing>{index + 1}</Table.Cell>
              <Table.Cell>{item.text}</Table.Cell>
              <Table.Cell collapsing>
                <Button
                  color='green'
                  icon='edit'
                  onClick={() => this.handleOpenDetailForm(name, actions.edit, item)}
                />
                <Button
                  color='red'
                  icon='trash'
                  onClick={() => this.handleDeleteRow(item)}
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
      <Menu secondary>
        <Button
          style={{ marginLeft: 5 }}
          color="green"
          floated="right"
          icon="edit"
          content={`Thêm ${name}`}
          onClick={() => this.handleOpenDetailForm(name, actions.add)}
        />
        <Button
          style={{ marginLeft: 5 }}
          color="brown"
          floated="right"
          icon="folder"
          content={`Dùng ${name} mẫu`}
          onClick={() => this.getDefaultCatalogs(name)}
        />
      </Menu>
    </div>
  )

  render() {
    return (
      <div>
        {this.state.isWaiting ?
          <Dimmer active>
            <Loader>Đang kết nối...</Loader>
          </Dimmer> : null
        }
        {this.renderDetailForm()}
        <Grid columns={2} stackable divided>
          <Grid.Row>
            <Button
              style={{ marginLeft: 15 }}
              loading={this.state.isWaiting}
              primary
              floated="right"
              onClick={this.handleSave}
            >
              <Icon name="save" />Lưu
            </Button>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {this.renderCatalog(names.position, this.state.positions)}
            </Grid.Column>
            <Grid.Column>
              {this.renderCatalog(names.role, this.state.roles)}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  unitID: state.user.unit.unitID
});

export default connect(mapStateToProps)(Catalogs)