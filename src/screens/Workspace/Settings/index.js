import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tab } from "semantic-ui-react";
import { connect } from "react-redux";
import NotificationSystem from "react-notification-system";

// Components
import Catalogs from "./Catalogs";


class Settings extends Component {
  static propTypes = {
    setting: PropTypes.shape({
      positions: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
      })).isRequired,
      roles: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
      })).isRequired
    }).isRequired
  };

  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  render() {
    const resultHandlers = {
      successHandler: this.successHandler,
      errorHandler: this.errorHandler
    };
    const panes = [{ 
      menuItem: "Danh mục", 
      render: () => <Tab.Pane><Catalogs {...resultHandlers} positions={this.props.setting.positions} roles={this.props.setting.roles} /></Tab.Pane> 
    }];
    return (
      <div>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
        <Tab 
          color="blue" 
          menu={{ color: "teal", fluid: true, pointing: true, borderless: true }} 
          panes={panes} 
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  setting: state.setting
});

export default connect(mapStateToProps)(Settings);