import React, { Component } from "react";
import PropTypes from "prop-types";
import { Segment, Breadcrumb, Table, Icon, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import NotificationSystem from "react-notification-system";
import _ from "lodash";

//
import axios from "axios";

// services
import { RESOURCES } from "../../../libs/config";
import { exception } from "../../../libs/ga";
import { setBoGiaoDucDocument, setSoGiaoDucDocument } from "../../../redux/actions/documents";

const cities = require("../../../libs/cities.json");

class Documents extends Component {
  static propTypes = {
    setBoGiaoDucDocument: PropTypes.func.isRequired,
    setSoGiaoDucDocument: PropTypes.func.isRequired,
    documents: PropTypes.shape({
      bogiaoducData: PropTypes.object,
      sogiaoducData: PropTypes.object
    }).isRequired,
    user: PropTypes.shape({
      unit: PropTypes.shape({
        information: PropTypes.object
      }).isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      path: "",
    }
  }

  componentWillMount = () => {
    this.getBoGiaoDucDocument();
    this.getSoGiaoDucDocument();
  }

  getBoGiaoDucDocument = () => {
    axios.get(`${RESOURCES}/abot/documents/structure.json`).then(response => {
      if (response.status === 200 && response.data !== undefined)
        this.props.setBoGiaoDucDocument('bogiaoducData', response.data);
    }).catch((error) => {
      exception(error);
    });
  }

  getSoGiaoDucDocument = () => {
    const { sgd } = this.props.user.unit.information;
    axios.get(`${RESOURCES}/abot/documents/${sgd}/structure.json`).then(response => {
      this.props.setSoGiaoDucDocument('sogiaoducData', response.data);
    }).catch((error) => {
      exception(error);
    });
  }

  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  renderDownloadButton = (resourcePath, filePath) => (
    <Button animated>
      <Button.Content visible>Tải về</Button.Content>
      <Button.Content hidden>
        <a href={`${resourcePath}${filePath}`} download><Icon name="download" /></a>
      </Button.Content>
    </Button>
  )

  renderBreadcrumb = (path, provinceName) => {
    const divider = <Breadcrumb.Divider icon="right chevron" />;
    const all = [<Breadcrumb.Section link onClick={() => this.setState({ path: "" })}>Tất cả</Breadcrumb.Section>];
    if (path.length > 0) {
      all.push(divider);

      if (path[0] === 'bogiaoduc') {
        all.push(<Breadcrumb.Section link onClick={() => this.setState({ path: path[0] })}>Bộ Giáo dục và Đào tạo</Breadcrumb.Section>);
      } else {
        all.push(<Breadcrumb.Section link onClick={() => this.setState({ path: path[0] })}>Sở Giáo dục và Đào tạo {provinceName}</Breadcrumb.Section>);
      }
    }
    if (path.length > 1) {
      all.push(divider);

      if (path[1] === 'mn')
        all.push(<Breadcrumb.Section link>Mầm non</Breadcrumb.Section>);
      else if (path[1] === 'th')
        all.push(<Breadcrumb.Section link>Tiểu học</Breadcrumb.Section>);
      else if (path[1] === 'thcs')
        all.push(<Breadcrumb.Section link>Trung Học Cơ Sở</Breadcrumb.Section>);
      else 
        all.push(<Breadcrumb.Section link>Các tài liệu nghiệp vụ</Breadcrumb.Section>);
    }
    return <Breadcrumb>{all}</Breadcrumb>;
  }

  renderFiles = (files) => (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Số/Ký hiệu</Table.HeaderCell>
          <Table.HeaderCell>Tên</Table.HeaderCell>
          <Table.HeaderCell>Ngày ban hành</Table.HeaderCell>
          <Table.HeaderCell>Cơ quan ban hành</Table.HeaderCell>
          <Table.HeaderCell />
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {files.map((file, idx) => (
          <Table.Row key={idx}>
            <Table.Cell collapsing><Icon name="file" />{file.number}</Table.Cell>
            <Table.Cell collapsing>{file.name}</Table.Cell>
            <Table.Cell collapsing>{file.published}</Table.Cell>
            <Table.Cell collapsing>{file.issuer}</Table.Cell>
            <Table.Cell>{this.renderDownloadButton(`${RESOURCES}/abot/documents/`, file.path)}</Table.Cell>
          </Table.Row>
          )
        )}
      </Table.Body>
    </Table>
  );

  renderDocuments = (path, provinceName) => {
    if (path.length === 0)
      return (
        <div>
          <Button color="teal" onClick={() => this.setState({ path: "bogiaoduc" })}>
            <Icon name="folder" /> Bộ Giáo dục và đào tạo
          </Button>
          <Button color="teal" onClick={() => this.setState({ path: "sogiaoduc" })}>
            <Icon name="folder" /> Sở Giáo dục và đào tạo {provinceName}
          </Button>
        </div>
      );
    if (path.length === 1)
      if (this.state.path === "bogiaoduc")
        return (
          <div>
            <Button color="teal" onClick={() => this.setState({ path: "bogiaoduc/mn" })}>
              <Icon name="folder" /> Mầm non
            </Button>
            <Button color="teal" onClick={() => this.setState({ path: "bogiaoduc/th" })}>
              <Icon name="folder" /> Tiểu học
            </Button>
            <Button color="teal" onClick={() => this.setState({ path: "bogiaoduc/thcs" })}>
              <Icon name="folder" /> Trung Học Cơ sở
            </Button>
          </div>
        );
      else 
        return (
          <div>
            <Button color="teal" onClick={() => this.setState({ path: "sogiaoduc/nghiepvu" })}>
              <Icon name="folder" /> Các tài liệu nghiệp vụ
            </Button>
          </div>
        );
    
    const { schoolType } = this.props.user.unit.information;
    if (this.state.path === `bogiaoduc/${schoolType}`)
      return this.renderFiles(this.props.documents.bogiaoducData[schoolType].files);
    return this.renderFiles(this.props.documents.sogiaoducData.nghiepvu.files);
  }

  render() {
    const path = _.compact(_.split(this.state.path, "/"));
    const { province } = this.props.user.unit.information;
    const provinceName = cities[province].name
    return (
      <div>
        <NotificationSystem ref={(ref) => { this.notificationSystem = ref }} />
        <Segment color="blue">{this.renderBreadcrumb(path, provinceName)}</Segment>
        <Segment>{this.renderDocuments(path, provinceName)}</Segment>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  documents: state.documents
});
const mapDispatchToProps = (dispatch) => bindActionCreators({ setBoGiaoDucDocument, setSoGiaoDucDocument }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Documents);