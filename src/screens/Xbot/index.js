import React, { Component } from "react";
import PropTypes from "prop-types";
import { Menu, Icon, List, Button } from "semantic-ui-react";
import NotificationSystem from "react-notification-system";
import { connect } from "react-redux";

// components
import Units from "./Units";
import UserHeader from "../../components/UserHeader";


class Xbot extends Component {
  static propTypes = {
    user: PropTypes.shape({ 
      uid: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      staff: PropTypes.object.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      error: "",
      menuActive: "units"
    };
  }

  componentWillMount() {
    if (this.props.user.staff.admin || this.props.user.staff.support)
      return;
    this.setState({
      error: `Chưa được cấu hình là tài khoản nhân viên. Xin liên hệ Admin và cung cấp địa chỉ Email: ${this.props.user.email}. Mã Tài Khoản: ${this.props.user.uid}`
    });
  }

  profileHandler = () => {
    this.notificationSystem.addNotification({
      title: "Thất bại",
      message: "Tính năng này không hỗ trợ ở màn hình quản lý.",
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  errorHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  renderCirculars = () => {
    const circulars = [{
      key: "mamnon",
      name: "Tiêu chuẩn đánh giá chất lượng giáo dục trường mầm non TT25"
    }, {
      key: "tieuhoc",
      name: "Tiêu chuẩn đánh giá chất lượng giáo dục tiểu học"
    }, {
      key: "trunghoc",
      name: "THPT, THCS, trường PT nhiều cấp, trường nội trú, trường bán trú"
    }, {
      key: "gdtx",
      name: "Tiêu chuẩn đánh giá chất lượng giáo dục trung tâm giáo dục thường xuyên"
    }];

    return (
      <List>
        <List.Item>
          <List.Icon size="big" color="brown" name="folder" />
          <List.Content>
            <List.Header>Thông tư</List.Header>
            <List.Description>Nội dung số hoá của tất cả các Thông tư sử dụng trong hệ thống ABOT</List.Description>
            <List divided verticalAlign="middle">{circulars.map(circular => (
              <List.Item key={circular.key}>
                <List.Content floated="right"><Button size="mini" icon="edit" color="blue" /></List.Content>
                <Icon size="big" name="file" color="brown" />
                <List.Content>{circular.name}</List.Content>
              </List.Item>
            ))}
            </List>
          </List.Content>
        </List.Item>
      </List>
    );
  }

  renderContent = () => {
    switch(this.state.menuActive) {
      case "units":
        return <Units />;
      case "circulars":
        return this.renderCirculars();
      default:
        return null;
    }
  }

  render() {
    if (this.state.error !== "")
      return <div>{this.state.error}</div>;
    return (
      <div>
        <Menu className="smallwrap" style={{ backgroundColor: "#208286" }} inverted secondary pointing icon="labeled">
          <Menu.Item name="units" active={this.state.menuActive === "units"} onClick={() => this.setState({ menuActive: "units" })}>
            <Icon name="write" />Đơn vị
          </Menu.Item>
          <Menu.Item name="circulars" active={this.state.menuActive === "circulars"} onClick={() => this.setState({ menuActive: "circulars" })}>
            <Icon name="tasks" />Tiêu chuẩn đánh giá
          </Menu.Item>
          <Menu.Menu position="right">
            <Menu.Item><UserHeader profileHandler={this.profileHandler} /></Menu.Item>
          </Menu.Menu>
        </Menu>
        <div className="smallwrap">
          {this.renderContent()}
        </div>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(Xbot);