import React, { Component } from "react";
import PropTypes from "prop-types";
import { Segment, Button, Dropdown, Message, Modal, Header, Icon, Dimmer, Loader, Label, Table, TableCell, Form } from "semantic-ui-react";
import NotificationSystem from "react-notification-system";
import { connect } from "react-redux";
import _ from "lodash";
import FileSaver from "file-saver";

// components
import Address from "./address";
import Unit from "./unit";

// services
import { db } from "../../../libs/firebase";
import { unitTypesOptions } from "../../../libs/units";


class Eduunits extends Component {
  static propTypes = {
    user: PropTypes.shape({
      email: PropTypes.string.isRequired,
      staff: PropTypes.object.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      selectedUnitType: undefined,
      units: undefined,
      selectedUnit: undefined
    }
  }

  searchHandler = () => {
    if (this.state.selectedUnitType === undefined) {
      this.errorHandler("Chưa chọn loại hình đơn vị cần tìm kiếm.");
      return;
    }

    if (this.addressRef === undefined)
      return;
    const { province, district, ward } = this.addressRef.getAddress();

    const where = { by: "province", cond: province };
    if (district !== undefined) {
      where.by = "district";
      where.cond = district;
    };
    if (ward !== undefined) {
      where.by = "ward";
      where.cond = ward;
    }

    switch(this.state.selectedUnitType) {
      case "sgd":
      case "pgd":
        if (province === undefined) {
          this.errorHandler("Chưa chọn Tỉnh");
          return;
        }
        break;
      case "th":
        if (district === undefined) {
          this.errorHandler("Chưa chọn Quận/Huyện");
          return;
        }
        break;
      default:
        this.errorHandler(`Chưa hỗ trợ tìm kiếm loại đơn vị ${this.state.selectedUnitType}`);
    }

    this.unitSearchHandler(this.state.selectedUnitType, where);
  }

  unitSearchHandler = (type, where) => {
    this.setState({ isWaiting: true, units: undefined, selectedUnit: undefined });
    db.collection("units").where("type","==",type).where(where.by,"==",where.cond).get().then(snapshot => {
      const units = [];
      snapshot.forEach(unit => {
        units.push({ ...unit.data(), key: unit.id, text: unit.data().name, value: unit.id });
      });
      this.setState({ isWaiting: false, units });
    }).catch(() => {
      this.setState({ isWaiting: false });
      this.errorHandler("Đã có lỗi khi kết nối dữ liệu hoặc tài khoản không đủ quyền để thực hiện thao tác này.");
    });
  }

  addUnitHandler = (unit) => {
    const type = (unit.type === "sgd")? "pgd" : "th";
    this.setState({ 
      unitEditor: { 
        name: (type === "pgd")? "Phòng Giáo dục" : "Trường học",
        type, 
        province: unit.province,
        district: unit.district,
        ward: unit.ward,
        sgd: unit.sgd,
        pgd: (type === "pgd")? undefined : unit.key
      } 
    });
  }

  errorHandler = (message) => {
    this.setState({ isWaiting: false });
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 4
    });
  }

  successHandler = (message) => {
    this.setState({ isWaiting: false });
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message,
      level: "success",
      position: "tr",
      autoDismiss: 4
    });
  }

  reloadHandler = (key, name) => {
    this.setState({ 
      unitEditor: {
        ...this.state.unitEditor,
        key,
        name
      }
    });
  }

  exportUnits = () => {
    this.setState({ isWaiting: true });
    const result = [];
    db.collection("units").get().then(units => {
      let count = 0;
      units.forEach(unit => {
        count += 1;
        if (unit.data().type === "th") {
          if (unit.data().ward !== undefined) {
            result.push({
              id: unit.id,
              name: unit.data().name,
              province: unit.data().province,
              district: unit.data().district,
              ward: unit.data().ward
            });
          } else
            console.log("unexpected", unit.data());
        }
        if (count === units.size) {
          this.setState({ isWaiting: false });
          const blobData = new Blob([JSON.stringify(result)], {type:"application/json"});
          FileSaver.saveAs(blobData, "units.json");
        }
      });
    })
  }

  renderUnitEditor = () => (
    <Modal size="fullscreen" className="custom" open={this.state.unitEditor !== undefined} closeIcon={<Icon name="close" onClick={() => this.setState({ unitEditor: undefined })} />}>
      <Header 
        className="form-header" 
        as="h3" 
        content={`${(this.state.unitEditor.key === undefined)? "Thêm mới" : "" } ${this.state.unitEditor.name}`} 
        subheader={(this.state.unitEditor.key !== undefined)? <Label color="teal"><Icon name="key" /> {this.state.unitEditor.key}</Label> : null} 
      />
      <Modal.Content scrolling>
        <Unit 
          unit={this.state.unitEditor} 
          errorHandler={this.errorHandler} 
          successHandler={this.successHandler} 
          reloadHandler={this.reloadHandler} 
          user={this.props.user}
        />
      </Modal.Content>
    </Modal>
  )

  renderUnits = () => (
    <Segment stacked>
      <Message info>
        <Message.Header>Kết quả tìm được</Message.Header>
        <p style={{width:300}}>Số đơn vị đã tạo: {this.state.units.length} </p>
      </Message>
      <Form>
        <Form.Field>
          <label>Tìm theo tên: </label>
          <Dropdown 
            search 
            selection 
            fluid 
            multiple={false} 
            onChange={(event,{value}) => this.setState({ selectedUnit: value })} 
            options={this.state.units} 
            value={this.state.selectedUnit} 
            placeholder="Nhập tên đơn vị" 
          />
        </Form.Field>
      </Form>
    </Segment>
  )

  renderResults = () => {
    const filter = _.filter(this.state.units, (unit) => this.state.selectedUnit === undefined || unit.key === this.state.selectedUnit);

    return (
      <div className="resTable">
        <Table unstackable celled striped>
          <Table.Header style={{textAlign: "center"}}>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell>Tên đơn vị</Table.HeaderCell>
              <Table.HeaderCell>Địa chỉ</Table.HeaderCell>
              <Table.HeaderCell width="1">Sửa</Table.HeaderCell>
              {(this.state.selectedUnitType !== "th")?
                <Table.HeaderCell width="1">Thêm</Table.HeaderCell> : null
              }
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {filter && filter.map((unit, idx) => (
              <Table.Row key={unit.key}>
                <TableCell>{idx + 1}</TableCell>
                <TableCell>{unit.name}</TableCell>
                <TableCell>{unit.address}</TableCell>
                <TableCell width="1"><Button color="brown" floated="right" icon="edit" onClick={() => this.setState({ unitEditor: unit })} /></TableCell>
                {(this.state.selectedUnitType === "pgd")?
                  <TableCell width="1"><Button color="teal" floated="right" icon="add" onClick={() => this.addUnitHandler(unit)} /></TableCell>
                  : null
                }
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </div>
    );
  }

  renderSearchResults = () => {
    if (this.state.units === undefined)
      return null;
    if (this.state.units.length === 0)
      return <Message color="teal">Không tìm thấy đơn vị nào trong hệ thống thoả điều kiện tìm kiếm.</Message>;
    return (
      <Segment>
        {this.renderUnits()}
        {this.renderResults()}
      </Segment>
    )
  }

  renderAdminMenu = () => (
    <Segment stacked>
      <Button floated="right" onClick={this.exportUnits}>Xuất danh mục đơn vị</Button>
    </Segment>
  )

  render() {
    return (
      <div>
        {this.state.isWaiting?
          <Dimmer active inverted>
            <Loader inverted>Đang kết nối...</Loader>
          </Dimmer> 
          : null
        }
        {this.props.user.staff.admin? this.renderAdminMenu() : null}
        {(this.state.unitEditor !== undefined)? this.renderUnitEditor() : null}
        <Segment stacked>
          <Form style={{ margin: 10 }}>
            <Form.Field>
              <label>Loại đơn vị</label>
              <Dropdown label="Loại đơn vị: " selection multiple={false} onChange={(event, { value }) => this.setState({ selectedUnitType: value })} options={unitTypesOptions} value={this.state.selectedUnitType} placeholder="Cần phải chọn loại đơn vị" />
            </Form.Field>
            <Address ref={ref => { this.addressRef = ref; }} />
          </Form>
          <Button.Group>
            <Button className="right-bottom-button" onClick={this.searchHandler}><Icon name="search" /> Tìm kiếm</Button>
          </Button.Group>
        </Segment>
        {this.renderSearchResults()}
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(Eduunits);