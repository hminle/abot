import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Radio, Input, Divider, Button, Dimmer, Loader, Table, Icon, Dropdown } from "semantic-ui-react";
import _ from "lodash";

// components
import Address from "./address";

// services
import { schoolTypesOptions } from "../../../libs/units";
import { db } from "../../../libs/firebase";


const uuid = require("uuid/v1");

export default class Unit extends Component {
  static propTypes = {
    errorHandler: PropTypes.func.isRequired,
    successHandler: PropTypes.func.isRequired,
    reloadHandler: PropTypes.func.isRequired,
    unit: PropTypes.shape({
      key: PropTypes.string,                 
      type: PropTypes.string.isRequired,  
      schoolType: PropTypes.string,    
      name: PropTypes.string.isRequired,   
      address: PropTypes.string,
      phone: PropTypes.string,
      note: PropTypes.string,
      province: PropTypes.string,           
      district: PropTypes.string,               
      ward: PropTypes.string,           
      pgd: PropTypes.string,         
      sgd: PropTypes.string,    
      staffs: PropTypes.object
    }).isRequired,
    user: PropTypes.shape({
      email: PropTypes.string.isRequired
    }).isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      isWaiting: false,
      unitID: (this.props.unit.key !== undefined)? this.props.unit.key : uuid(),
      type: this.props.unit.type,
      schoolType: (this.props.unit.schoolType !== undefined)? this.props.unit.schoolType : "",
      name: this.props.unit.name,
      address: (this.props.unit.address === undefined)? "" : this.props.unit.address,
      phone: (this.props.unit.phone === undefined)? "" : this.props.unit.phone,
      note: (this.props.unit.note === undefined)? "" : this.props.unit.note,
      staffs: (this.props.unit.staffs === undefined)? {} : this.props.unit.staffs,
      staffEmail: ""
    }
  }

  validate = (province, district, ward) => {
    if (this.state.name === "") {
      this.props.errorHandler("Tên đơn vị không được bỏ trống");
      return false;
    }
    if (this.state.address === "") {
      this.props.errorHandler("Địa chỉ đơn vị không được bỏ trống");
      return false;
    }
    if (this.state.type === "pgd" && district === undefined) {
      this.props.errorHandler("Thông tin Quận/Huyện không được bỏ trống");
      return false;
    }
    if (this.state.type === "th") {
      if (ward === undefined) {
        this.props.errorHandler("Thông tin Phường/Xã không được bỏ trống");
        return false;
      }
      if (this.state.schoolType === "") {
        this.props.errorHandler("Thông tin về loại trường không được bỏ trống");
        return false;
      }
    }
    return true;
  }

  saveHandler = () => {
    if (this.addressRef === undefined)
      return;
    const { province, district, ward } = this.addressRef.getAddress();
    if (this.validate(province, district, ward)) {
      this.setState({ isWaiting: true });

      const updateContent = {
        type: this.state.type,
        name: this.state.name,
        address: this.state.address,
        phone: this.state.phone,
        note: this.state.note,
        staffs: this.state.staffs,
        province,
        status: {
          time: new Date().getTime(),
          by: this.props.user.email
        }
      };

      if (this.state.type !== "sgd")
        updateContent.district = district;
      if (this.state.type === "th") {
        updateContent.ward = ward;
        updateContent.schoolType = this.state.schoolType;
      }

      if (this.props.unit.sgd !== undefined)
        updateContent.sgd = this.props.unit.sgd
      if (this.props.unit.pgd !== undefined)
        updateContent.pgd = this.props.unit.pgd

      // try to validate uid first
      if (this.props.unit.key === undefined) {
        db.collection("units").doc(this.state.unitID).get().then(snapshot => {
          if (snapshot.exists) {
            this.setState({ unitID: uuid(), isWaiting: false });
            this.props.errorHandler("Mã ID bị trùng lặp. Xin vui lòng thử lại sau.");
          } else
            this.doSave(updateContent);
        });
      } else
        this.doSave(updateContent);
    }
  }

  doSave = (updateContent) => {
    db.collection("units").doc(this.state.unitID).set(updateContent).then(() => {
      if (_.keys(updateContent.staffs).length === 0) {
        this.setState({ isWaiting: false });
        this.props.successHandler("Cập nhật thông tin đơn vị thành công.");
      }
      // set active unit for users
      _.keys(updateContent.staffs).forEach((staffEmail, index) => {
        db.collection("users").doc(staffEmail).set({
          activeUnit: this.state.unitID
        }, { mergeFields: ["activeUnit"] }).then(() => {
          if (index === _.keys(updateContent.staffs).length-1) {
            this.setState({ isWaiting: false });
            this.props.successHandler("Cập nhật thông tin đơn vị thành công.");
          }
        }).catch((error) => {
          console.log(error);
          if (index === _.keys(updateContent.staffs).length-1)
            this.setState({ isWaiting: false });
          this.props.errorHandler(`Cập nhật thông tin đơn vị cho tài khoản ${staffEmail} thất bại.`);
        })
      });
      this.props.reloadHandler(this.state.unitID, this.state.name);
    }).catch(() => {
      this.setState({ isWaiting: false })
      this.props.errorHandler("Cập nhật đơn vị thất bại. Xin vui lòng thử lại sau.");
    });
  }

  removeEmailHandler = (email) => {
    const rest = _.filter(_.keys(this.state.staffs), (staff) => staff !== email);
    this.setState({ staffs: _.pick(this.state.staffs, rest )});
  }

  newEmailHandler = () => {
    if (this.state.staffEmail === "" || !(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))(\.*)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.staffEmail))) {
      this.props.errorHandler("Địa chỉ Email không hợp lệ.");
      return;
    }
    this.setState({ 
      staffs: {...this.state.staffs, [this.state.staffEmail]: "admin"},
      staffEmail: ""
    });
  }

  renderStaff = (staff, index) => {
    if (staff.email !== "")
      return (
        <Table.Row key={index} positive={staff.role === "admin"}>
          <Table.Cell>{staff.email}</Table.Cell>
          <Table.Cell width="1">
            <Button color="red" onClick={() => this.removeEmailHandler(staff.email)}>
              <Icon name="trash" />
            </Button>
          </Table.Cell>
        </Table.Row>
      );
    return (
      <Table.Row key={index} positive={this.state.staffRole === "admin"}>
        <Table.Cell><Input value={(this.state.staffEmail !== undefined)? this.state.staffEmail : ""} onChange={(event,{value}) => this.setState({ staffEmail: value })} /></Table.Cell>
        <Table.Cell width="1">
          <Button color="teal" onClick={this.newEmailHandler}>
            <Icon name="add" />
          </Button>
        </Table.Cell>
      </Table.Row>
    );
  }

  renderStaffs = () => {
    const emails = [];
    _.keys(this.state.staffs).forEach(email => emails.push({ email, role: this.state.staffs[email] }));
    emails.push({ email: "", role: "admin" });

    return (
      <Table unstackable celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell width="1" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {emails.map((staff, index) => this.renderStaff(staff, index))}
        </Table.Body>
      </Table>
    );
  }

  render() {
    return (
      <div>
        {this.state.isWaiting?
          <Dimmer active inverted>
            <Loader inverted>Đang kết nối...</Loader>
          </Dimmer> 
          : null
        }
        <Form>
          <Address ref={ref => { this.addressRef = ref; }} province={this.props.unit.province} district={this.props.unit.district} ward={this.props.unit.ward} />
          <Form.Group inline>
            <label>Loại đơn vị: </label>
            <Form.Field control={Radio} label="Sở Giáo dục" value="sgd" checked={this.state.type === "sgd"} onChange={() => this.setState({ type: "sgd" })} />
            <Form.Field control={Radio} label="Phòng Giáo dục" value="pgd" checked={this.state.type === "pgd"} onChange={() => this.setState({ type: "pgd" })} />
            <Form.Field control={Radio} label="Trường học" value="th" checked={this.state.type === "th"} onChange={() => this.setState({ type: "th" })} />
          </Form.Group>
          {(this.state.type === "th")?
            <Form.Field required>
              <label>Loại trường</label>
              <Dropdown
                search 
                selection 
                multiple={false} 
                onChange={(event, { value }) => this.setState({ schoolType: value })} 
                options={schoolTypesOptions} 
                value={this.state.schoolType} 
                placeholder="Chọn loại trường"
              />
            </Form.Field> : null
          }
          <Form.Field required>
            <label>Tên đơn vị</label>
            <Input placeholder="Tên đơn vị" value={this.state.name} onChange={(event, {value}) => this.setState({ name: value })} />
          </Form.Field>
          <Form.Group>
            <Form.Field required width="11">
              <label>Địa chỉ</label>
              <Input placeholder="Địa chỉ" value={this.state.address} onChange={(event, {value}) => this.setState({ address: value })} />
            </Form.Field>
            <Form.Field width="5">
              <label>SĐT</label>
              <Input placeholder="SĐT" value={this.state.phone} onChange={(event, {value}) => this.setState({ phone: value })} />
            </Form.Field>
          </Form.Group>
          <Form.Field>
            <label>Ghi chú</label>
            <Form.TextArea placeholder="Ghi chú" value={this.state.note} onChange={(event, {value}) => this.setState({ note: value })} />
          </Form.Field>
          <Divider />
          <Form.Field>
            <label>Admin/Hiệu trưởng</label>
            {this.renderStaffs()}
          </Form.Field>
          <Button style={{float: "right", marginBottom: "10px"}} color="brown" onClick={this.saveHandler}>Cập nhật</Button>
        </Form>
      </div>
    )
  }
}