import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Dropdown } from "semantic-ui-react";
import _ from "lodash";


const cities = require("../../../libs/cities.json");

export default class Address extends Component {
  static propTypes = {
    province: PropTypes.string,
    district: PropTypes.string,
    ward: PropTypes.string,
    locked: PropTypes.bool
  };

  static defaultProps = {
    locked: false
  }

  static defaultProps = {
    province: undefined,
    district: undefined,
    ward: undefined
  }

  constructor(props) {
    super(props);

    this.state = {
      provinces: [],
      districts: [],
      wards: [],
      selectedProvince: undefined,
      selectedDistrict: undefined,
      selectedWard: undefined
    }
  }

  componentWillMount() {
    const provinces = [];
    const districts = [];
    const wards = [];

    Object.keys(cities).forEach(key => provinces.push({ key, text: cities[key].name, value: key }));

    if (this.props.province !== undefined) {
      Object.keys(cities[this.props.province].districts).forEach(key => 
        districts.push({ key, text: cities[this.props.province].districts[key].name, value: key }));
      if (this.props.district !== undefined)
        Object.keys(cities[this.props.province].districts[this.props.district].wards).forEach(key =>
          wards.push({ key, text: cities[this.props.province].districts[this.props.district].wards[key].name, value: key }));
    }

    this.setState({ 
      provinces: _.orderBy(provinces, ["text"], ["asc"]), 
      districts: _.orderBy(districts, ["text"], ["asc"]), 
      wards: _.orderBy(wards, ["text"], ["asc"]), 
      selectedProvince: this.props.province, 
      selectedDistrict: this.props.district,
      selectedWard: this.props.ward 
    });
  }

  getAddress = () => ({
    province: this.state.selectedProvince,
    district: this.state.selectedDistrict,
    ward: this.state.selectedWard
  })

  provinceHandler = (event, { value }) => {
    this.setState({ selectedProvince: value, districts: [], selectedDistrict: undefined });
    const districts = [];
    Object.keys(cities[value].districts).forEach(key => districts.push({ key, text: cities[value].districts[key].name, value: key }));
    this.setState({ districts: _.orderBy(districts, ["text"], ["asc"]) });
  }

  districtHandler = (event, { value }) => {
    console.log(value);
    this.setState({ selectedDistrict: value, wards: [], selectedWard: undefined });
    const wards = [];
    Object.keys(cities[this.state.selectedProvince].districts[value].wards).forEach(key =>
      wards.push({ key, text: cities[this.state.selectedProvince].districts[value].wards[key].name, value: key }));
    this.setState({ wards: _.orderBy(wards, ["text"], ["asc"]) });
  }

  wardHandler = (event, { value }) => {
    this.setState({ selectedWard: value });
  }

  renderProvince = () => (
    <Form.Field>
      <label>Tỉnh</label>
      <Dropdown 
        label="Tỉnh: " 
        search 
        selection 
        multiple={false} 
        onChange={this.provinceHandler} 
        options={this.state.provinces} 
        value={this.state.selectedProvince} 
        placeholder="Nhập tên Tỉnh" 
        disabled={this.props.province !== undefined || this.props.locked}
      />
    </Form.Field>
  )

  renderDistrict = () => (
    <Form.Field>
      <label>Quận/Huyện</label>
      <Dropdown 
        label="Quận: " 
        search 
        selection 
        multiple={false} 
        onChange={this.districtHandler} 
        options={this.state.districts} 
        value={this.state.selectedDistrict} 
        placeholder="Nhập tên Quận Huyện" 
        disabled={this.props.district !== undefined || this.props.locked}
      />
    </Form.Field>
  )

  renderWard = () => (
    <Form.Field>
      <label>Phường/Xã</label>
      <Dropdown 
        label="Phường: " 
        search 
        selection 
        multiple={false} 
        onChange={this.wardHandler} 
        options={this.state.wards} 
        value={this.state.selectedWard} 
        placeholder="Nhập tên Phường Xã"
        disabled={this.props.locked}
      />
    </Form.Field>
  )

  render() {
    return (
      <Form.Group width="equal">
        {this.renderProvince()}
        {this.renderDistrict()}
        {this.renderWard()}
      </Form.Group>
    );
  }
}