import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";


const sgd = "dongnai";

const config = {   
  apiKey: "AIzaSyBw7X5JR2nTp8yVeHoGXm5FMNHJCbKYHnE",
  authDomain: "abot-dev.firebaseapp.com",
  databaseURL: "https://abot-dev.firebaseio.com",
  projectId: "abot-dev",
  storageBucket: "abot-dev.appspot.com",
  messagingSenderId: "776509828093"
};

const app = firebase.initializeApp(config);
app.auth().languageCode = "vi";

app.firestore().settings({
  timestampsInSnapshots: true
});
const db = app.firestore();

const googleProvider = new firebase.auth.GoogleAuthProvider().setCustomParameters({
  prompt: "select_account"
});

const deleteField = firebase.firestore.FieldValue.delete();

export { sgd, app, db, googleProvider, deleteField }