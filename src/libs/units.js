export const unitTypesOptions = [
  { key: "sgd", text: "Sở Giáo dục", value: "sgd" },
  { key: "pgd", text: "Phòng Giáo dục", value: "pgd" },
  { key: "th", text: "Trường học", value: "th" }
];

export const schoolTypesOptions = [
  { key: "mn", text: "Trường Mầm non", value: "mn" },
  { key: "th", text: "Trường Tiểu học", value: "th" },
  { key: "thcs", text: "Trường Trung học cơ sở", value: "thcs" },
  { key: "thpt", text: "Trường Trung học phổ thông", value: "thpt" },
  { key: "lc12", text: "Trường liên cấp 1, 2", value: "lc12" },
  { key: "lc23", text: "Trường liên cấp 2, 3", value: "lc23" },
  { key: "pt_dtnt", text: "Trường Phổ thông Dân tộc nội trú", value: "pt_dtnt" },
  { key: "gdtx", text: "Trung tâm Giáo dục thường xuyên", value: "gdtx" },
]