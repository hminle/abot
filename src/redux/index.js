import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from 'redux-thunk'
import reducer from "./reducers";

const ReduxThunkMiddleware = applyMiddleware(ReduxThunk);

/* eslint-disable no-underscore-dangle */
const store = createStore(
  reducer,
  compose(
    ReduxThunkMiddleware,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
/* eslint-enable */

export default store;