import { EVALUATION } from "./constants";


/**
 * Fetch evaluation
 * @param  {String} [key]
 * @param  {Object} [value] as firebase
 */
export function fetchEvaluation(key, value) {
  return {
    type: EVALUATION,
    key, value
  };
}