import { BOGIAODUC_DOCUMENT, SOGIAODUC_DOCUMENT } from "./constants";


/**
 * Fetch setting
 * @param  {String} [key]
 * @param  {Object} [value] as json
 */
export function setBoGiaoDucDocument(key, value) {
  return {
    type: BOGIAODUC_DOCUMENT,
    key, value
  };
}


/**
 * Fetch setting
 * @param  {String} [key]
 * @param  {Object} [value] as json
 */
export function setSoGiaoDucDocument(key, value) {
    return {
      type: SOGIAODUC_DOCUMENT,
      key, value
    };
  }