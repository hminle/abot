import { USER_FETCH, USER_IDENTIFY, SIGN_OUT, USER_UNIT, USER_STAFF } from "./constants";


/**
 * Fetch general information of a user
 * @param  {String} [displayName]
 * @param  {String} [email]
 * @param  {String} [phoneNumber]
 * @param  {String} [photoURL]
 * @param  {String} [uid]
 */
export function userFetchInformation(displayName, email, phoneNumber, photoURL, uid) {
  const name = (displayName === null)? "" : displayName;
  const phone = (phoneNumber === null)? "" : phoneNumber;
  const avatar = (photoURL === null)? "" : photoURL;
  return {
    type: USER_FETCH,
    displayName: name,
    email,
    phoneNumber: phone,
    photoURL: avatar,
    uid
  };
}

/**
 * Fetch detail information of a user
 * @param  {String} [displayName]
 * @param  {String} [phoneNumber]
 * @param  {String} [photoURL]
 * @param  {String} [gender] "M" or "F", could be undefined
 */
export function userIdentify(displayName, phoneNumber, photoURL, gender) {
  const name = (displayName === undefined)? "" : displayName;
  const phone = (phoneNumber === undefined || phoneNumber === null)? "" : phoneNumber;
  const avatar = (photoURL === undefined)? "" : photoURL;
  return {
    type: USER_IDENTIFY,
    displayName: name,
    phoneNumber: phone,
    photoURL: avatar,
    gender
  };
}

/**
 * Fetch user's active unit information
 * @param  {String} [unitID]
 * @param  {Object} [unitInformation]
 */
export function userFetchUnit(unitID, unitInformation) {
  return {
    type: USER_UNIT,
    unitID,
    unitInformation
  };
}

export function signOut() {
  return {
    type: SIGN_OUT
  };
}

/**
 * Fetch staffs role
 * @param  {Object} [role]
 */
export function userStaff(role) {
  return {
    type: USER_STAFF,
    role
  };
}