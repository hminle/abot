import { SETTINGS } from "./constants";


/**
 * Fetch setting
 * @param  {String} [key]
 * @param  {Object} [value] as firebase
 */
export function fetchSetting(key, value) {
  return {
    type: SETTINGS,
    key, value
  };
}