export const USER_FETCH = "USER_FETCH";
export const USER_IDENTIFY = "USER_IDENTIFY";
export const USER_UNIT = "USER_UNIT";
export const USER_STAFF = "USER_STAFF";

export const SETTINGS = "SETTINGS";

export const SIGN_OUT = "SIGN_OUT";

export const BOGIAODUC_DOCUMENT = "BOGIAODUC_DOCUMENT";
export const SOGIAODUC_DOCUMENT = "SOGIAODUC_DOCUMENT";

export const EVALUATION = "EVALUATION";
