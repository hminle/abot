import { combineReducers } from "redux";

import user from "./user";
import setting from "./setting";
import documents from "./documents";
import evaluation from "./evaluation";


const rootReducer = combineReducers({
  user,
  setting,
  documents,
  evaluation
});

export default rootReducer;