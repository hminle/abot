import { USER_FETCH, USER_IDENTIFY, USER_UNIT, USER_STAFF, SIGN_OUT } from "../actions/constants";


const initialState = {
  displayName: "",
  email: "",
  phoneNumber: "",
  photoURL: "",
  gender: undefined,
  uid: "",
  unit: {},
  staff: {}
};

export default function user(state = initialState, action) {
  switch(action.type) {
    case USER_FETCH:
      return {
        ...state,
        displayName: action.displayName,
        email: action.email,
        phoneNumber: action.phoneNumber,
        photoURL: action.photoURL,
        uid: action.uid
      };
    case USER_IDENTIFY:
      return {
        ...state,
        displayName: action.displayName,
        phoneNumber: action.phoneNumber,
        photoURL: action.photoURL,
        gender: action.gender
      };
    case USER_UNIT:
      return {
        ...state,
        unit: {
          unitID: action.unitID,
          information: action.unitInformation
        }
      };
    case USER_STAFF:
      return {
        ...state,
        staff: action.role
      };
    case SIGN_OUT:
      return initialState;
    default:
      return state;
  }
}