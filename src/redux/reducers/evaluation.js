import { EVALUATION, SIGN_OUT } from "../actions/constants";


const initialState = {};

export default function evaluation(state = initialState, action) {
  switch(action.type) {
    case EVALUATION:
      return {
        ...state,
        [action.key]: action.value
      };
    case SIGN_OUT:
      return initialState;
    default:
      return state;
  }
}