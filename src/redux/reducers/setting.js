import { SETTINGS, SIGN_OUT } from "../actions/constants";


const initialState = {
  positions: [],
  roles: []
};

export default function setting(state = initialState, action) {
  switch(action.type) {
    case SETTINGS:
      return {
        ...state,
        [action.key]: action.value
      };
    case SIGN_OUT:
      return initialState;
    default:
      return state;
  }
}