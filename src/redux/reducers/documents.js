import { BOGIAODUC_DOCUMENT, SOGIAODUC_DOCUMENT, SIGN_OUT } from "../actions/constants";


const initialState = {
  bogiaoducData: {},
  sogiaoducData: {}
};

export default function documents(state = initialState, action) {
  switch(action.type) {
    case BOGIAODUC_DOCUMENT:
      return {
        ...state,
        [action.key]: action.value
      };
    case SOGIAODUC_DOCUMENT:
      return {
        ...state,
        [action.key]: action.value  
      }
    case SIGN_OUT:
      return initialState;
    default:
      return state;
  }
}