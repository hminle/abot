import React, { Component } from "react";
import PropTypes from "prop-types";
import { Container, Step, Icon, Message, Dimmer, Loader } from "semantic-ui-react";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import NotificationSystem from "react-notification-system";

// components
import Welcome from "./screens/Welcome";
import Workspace from "./screens/Workspace";
import Xbot from "./screens/Xbot";
import PrivateRoute from "./PrivateRoute";

// styles
import "./App.css";

// services
import packageJson from "../package.json";
import { exception } from "./libs/ga";
import { app, db } from "./libs/firebase";
import { userFetchInformation, userIdentify, userFetchUnit, userStaff } from "./redux/actions/users";


class App extends Component {
  static propTypes = {
    userFetchInformation: PropTypes.func.isRequired,
    userIdentify: PropTypes.func.isRequired,
    userFetchUnit: PropTypes.func.isRequired,
    userStaff: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: false,
      identify: false,
      error: "",
      isWaiting: false
    };
  }

  componentWillMount() {
    this.removeAuthListener = app.auth().onAuthStateChanged((user) => {
      if (user) {
        this.props.userFetchInformation(user.displayName, user.email, user.phoneNumber, user.photoURL, user.uid);
        this.setState({ user: true }, () => {
          db.collection("users").doc(user.email).get()
          .then((data) => this.readUserInfoSuccess(data, user))
          .catch(error => this.readInfoFailed(error, "Đọc thông tin người dùng không thành công."));
        });
      } else
        this.setState({ user: true, identify: true });
    });
  }
  
  componentWillUnmount() {
    this.removeAuthListener();
  }

  mergeInformation = (original, data, field) => {
    if (data[field] === undefined)
      return original[field];
    return data[field];
  }

  readUserInfoSuccess = (user, extra) => {
    db.collection("staffs").doc(extra.email).get().then((role) => {
      if (role.exists)
        this.props.userStaff(role.data());
      if (user.exists) {
        this.props.userIdentify(
          this.mergeInformation(extra, user.data(), "displayName"), 
          this.mergeInformation(extra, user.data(), "phoneNumber"),
          this.mergeInformation(extra, user.data(), "photoURL"),
          user.data().gender
        );
        if (user.data().activeUnit !== undefined)
          db.collection("units").doc(user.data().activeUnit).get().then(unitInformation => {
            if (unitInformation.exists) {
              this.props.userFetchUnit(user.data().activeUnit, unitInformation.data());
              this.setState({ identify: true });
            } else
              this.setState({ identify: true });
          }).catch(() => this.setState({ identify: true }));
        else
          this.setState({ identify: true });
      } else {
        db.collection("users").doc(extra.email).set({
          displayName: extra.displayName,
          phoneNumber: (extra.phoneNumber === undefined || extra.phoneNumber === null)? "" : extra.phoneNumber,
          photoURL: extra.photoURL
        }, { mergeFields: ["displayName","phoneNumber","photoURL"] }).then(() => {
          this.setState({ identify: true });
        }).catch(error => this.readInfoFailed(error, "Cập nhật thông tin người dùng không thành công."));
      }
    }).catch(error => this.readInfoFailed(error, "Đọc thông tin người dùng không thành công.")); 
  }

  readInfoFailed = (error, msg) => {
    exception(error);
    this.setState({
      error: msg
    });
    app.auth().signOut();
  }

  errorHander = (message) => {
    this.notificationSystem.addNotification({
      title: "Lỗi",
      message,
      level: "error",
      position: "tr",
      autoDismiss: 2
    });
  }

  renderProgress = () => (
    <Container textAlign="center">
      <Step.Group>
        <Step active={!this.state.user} completed={this.state.user} style={{background: "transparent"}}>
          <Icon loading={!this.state.user} name="server" />
          <Step.Content>
            <Step.Title>Kiểm tra kết nối</Step.Title>
            <Step.Description>Kết nối đến cơ sở dữ liệu người dùng</Step.Description>
          </Step.Content>
        </Step>
        <Step active={!this.state.identify} completed={this.state.identify} style={{background: "transparent"}}>
          <Icon loading={!this.state.identify} name="user" />
          <Step.Content>
            <Step.Title>Xác thực người dùng</Step.Title>
            <Step.Description>Kiểm tra thông tin người dùng</Step.Description>
          </Step.Content>
        </Step>
      </Step.Group>
    </Container>
  )
  
  render() {
    if (this.state.error !== "")
      return (
        <Container textAlign="center">
          <Message
            error
            header={this.state.error}
            list={[
              "Truy cập lại hệ thống ở tab khác hoặc vào thời điểm khác.",
              "Liên hệ đến công ty để được hỗ trợ.",
            ]}
          />
        </Container>
      );
    if (!this.state.user || !this.state.identify)
      return this.renderProgress();

    return (
      <BrowserRouter>
        <div>
          <div style={{ background: "rgba(252, 252, 252, 0.32)", paddingBottom: 50 }}>
            {this.state.isWaiting?
              <Dimmer active>
                <Loader>Đang kết nối...</Loader>
              </Dimmer> : null
            }
            <Route exact path="/" component={Welcome} />
            <PrivateRoute path="/workspace" component={Workspace} />
            <PrivateRoute path="/xbot" component={Xbot} />
          </div>
          <div style={{ position: "fixed", right: 0, bottom: 0, left: 0, padding: "1rem", textAlign: "center", background: "#F2F2F2", fontFamily: `sans-serif, Arial, Verdana, "Trebuchet MS"` }}>
            <center>ABOT v{packageJson.version} - © 2018, XBOT Technology JSC. All Rights Reserved.</center>
          </div>
          <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ 
  userFetchInformation, userIdentify, userFetchUnit, userStaff
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);