import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Button, Input, Dimmer, Loader, Dropdown, Divider, Message } from "semantic-ui-react";
import NotificationSystem from "react-notification-system";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// services
import { app, db, deleteField } from "../../libs/firebase";
import { userIdentify, signOut } from "../../redux/actions/users";


class ProfileForm extends Component {
  static propTypes = {
    signOut: PropTypes.func.isRequired,
    userIdentify: PropTypes.func.isRequired,
    closeHandler: PropTypes.func.isRequired,
    forceUpdate: PropTypes.bool,
    messages: PropTypes.arrayOf(PropTypes.string),
    user: PropTypes.shape({
      uid: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      phoneNumber: PropTypes.string,
      photoURL: PropTypes.string.isRequired,
      gender: PropTypes.string,
      staff: PropTypes.object.isRequired
    }).isRequired
  };

  static defaultProps = {
    forceUpdate: false,
    messages: undefined
  }

  constructor(props) {
    super(props);

    this.state = {
      isWaiting: false,
      forceUpdate: this.props.forceUpdate,
      displayName: this.props.user.displayName,
      phoneNumber: this.props.user.phoneNumber,
      gender: this.props.user.gender,
      messages: this.props.messages
    };
  }

  validateInformation = () => {
    if (this.state.displayName === "") {
      this.errorHandler("Thông tin không hợp lệ", "Họ và tên không được bỏ trống");
      return false;
    }
    if (this.state.gender === undefined) {
      this.errorHandler("Thông tin không hợp lệ", "Giới tính không được bỏ trống");
      return false;
    }
    return true;
  }

  saveHandler = () => {
    if (this.validateInformation()) {
      this.setState({ isWaiting: true });
      db.collection("users").doc(this.props.user.email).update({
        displayName: this.state.displayName,
        phoneNumber: this.state.phoneNumber,
        gender: this.state.gender
      }).then(() => this.updateComplete("")).catch(() => {
        this.updateComplete("Cập nhật thông tin thất bại. Xin vui lòng thử lại sau.");
      });
    }
  }

  errorHandler = (title, message) => {
    this.notificationSystem.addNotification({
      title,
      message,
      level: "warning",
      position: "tr",
      autoDismiss: 2
    });
  }

  updateComplete = (error) => {
    if (error) {
      this.errorHandler("Lỗi", error);
      this.setState({
        isWaiting: false,
        messages: undefined
      });
      return;
    }
    this.props.userIdentify(this.state.displayName, this.state.phoneNumber, this.props.user.photoURL, this.state.gender);
    this.notificationSystem.addNotification({
      title: "Hoàn tất",
      message: "Cập nhật thành công",
      level: "success",
      position: "tr",
      autoDismiss: 2
    });
    this.setState({ isWaiting: false, forceUpdate: false, messages: undefined });
  }

  leaveUnit = () => {
    this.setState({ isWaiting: true });
    db.collection("users").doc(this.props.user.email).update({
      activeUnit: deleteField
    }).then(() => {
      this.logoutHandler();
    }).catch(() => {
      this.setState({ isWaiting: false });
      this.errorHandler("Lỗi", "Đã có lỗi khi kết nối tới hệ thống. Xin vui lòng thử lại sau.");
    })
  }

  logoutHandler = () => {
    app.auth().signOut().then(() => {
      this.props.signOut();
    });
  }

  render() {
    return (
      <div>
        <center>
          {this.state.isWaiting? 
            <Dimmer active inverted>
              <Loader inverted>Đang kết nối...</Loader>
            </Dimmer> 
            : null
          }
        </center>
        {(this.state.messages !== undefined)?
          this.state.messages.map((message, index) => <Message key={index} warning>{message}</Message>) : null
        }
        <Form>
          <Form.Field required>
            <label>Họ và tên</label>
            <Input
              value={this.state.displayName}
              onChange={(e, { value }) => this.setState({displayName: value})}
              icon="user"
              placeholder="Họ và tên"
              iconPosition="left"
            />
          </Form.Field>
          <Form.Field>
            <label>Số điện thoại</label>
            <Input 
              value={this.state.phoneNumber}
              onChange={(e, { value }) => this.setState({phoneNumber: value})}
              icon="phone"
              placeholder="Số điện thoại"
              iconPosition="left"
            />
          </Form.Field>
          <Form.Field required>
            <label>Giới tính</label>
            <Dropdown
              selection
              multiple={false}
              onChange={(event,{value}) => this.setState({ gender: value })}
              options={[{key: "M", text: "Nam", value: "M"}, {key: "F", text: "Nữ", value: "F"}]}
              defaultValue={this.state.gender}
              placeholder="Giới tính"
            />
          </Form.Field>

          <Divider />
          
          <div style={{ float: "right", paddingBottom: 10 }}>
            {this.state.isWaiting?
              <Button secondary>Đang cập nhật...</Button> 
              :
              <Button basic color="blue" onClick={this.saveHandler}>Cập nhật</Button> 
            }
            {this.props.user.staff.support? <Button basic color="red" onClick={this.leaveUnit}>Rời đơn vị</Button>  : null}
            {this.state.forceUpdate?
              <Button basic color="grey" onClick={this.logoutHandler}>Đăng xuất</Button>  : <Button basic color="grey" onClick={this.props.closeHandler}>Đóng</Button> 
            }
          </div>
        </Form>
        <NotificationSystem ref={(ref) => {this.notificationSystem = ref}} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  signOut, userIdentify
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileForm);